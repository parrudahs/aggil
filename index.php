<?php
    require __DIR__ . '/vendor/autoload.php';
    require __DIR__ . '/Jcms/setup/config.php';

    $urls = explode('/', isset($_GET['url']) ? $_GET['url'] : null);
    $requested_url = isset($urls[0]) ? $urls[0] : NULL;

    $requested_file = '';
    $content_file = '';

    if($urls[0] == "admin") {
        if(isset($urls[1]) && $urls[1] != null) {
            if(file_exists('view/admin/content/'.$urls[1].'.php')) {
                $content_file = $urls[1].".php";
            } else {
                $content_file = "404.php";
            }
            include('view/admin/base_struct.php');
        }else {
            include("view/admin/login.php");
        }
    } else if($urls[0] == "action") {
        if(isset($urls[1]) && is_dir('Jcms/Core/actions/'.$urls[1])) {
            if(isset($urls[2]) && file_exists("Jcms/Core/actions/".$urls[1]."/".$urls[2].".php")){
                include("Jcms/Core/actions/".$urls[1]."/".$urls[2].".php");
            } else{
                $requested_file = ERROR_404;
                include("view/site/base_struct.php");
            }
        }else {
            $requested_file = ERROR_404;
            include("view/site/base_struct.php");
        }
    } else {
        if(file_exists('view/site/content/'.$urls[0].'.php')) {
            $requested_file = $urls[0].".php";
        }else {
            if($urls[0] != NULL)
                $requested_file = ERROR_404;
            else
                $requested_file = "inicial.php";
        }
        include("view/site/base_struct.php");
    }

?>