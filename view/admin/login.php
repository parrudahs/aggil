<?php
$login = new Jcms\Core\Auth\Autenticacao();
if (isset($_POST['envia'])) {
    if ($login->login($_POST['login'], $_POST['senha']))
        header('Location:' . BASE_URL . 'admin/inicial');
}
if (isset($_SESSION['backend']['user_logged']) && $_SESSION['backend']['user_logged'])
    header('Location:' . BASE_URL . 'admin/inicial');
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title>:: Aggil Transportes - Transporte e Logística ::</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="<?= BASE_URL ?>public/libs/font-awesome/css/font-awesome.css"/>
        <link rel="stylesheet" href="<?= BASE_URL ?>public/libs/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="<?= BASE_URL ?>public/libs/bootstrap/css/default.css"/>
    </head>
    <body class="default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <br/>
                        <!-- Imagem ou logotipo da aplicação -->
                        <img src="<?= BASE_URL ?>public/images/logo.png" title="Aggil Transportes" alt="Aggil Transportes"/>
                        <!-- Título da aplicação -->
                        <h2>Aggil Transportes</h2>
                        <!-- Descrição da aplicação -->
                        <h4>Gerenciamento de conteúdo</h4>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-3 col-lg-3"></div>
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                    <br/>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>
                                <i class="fa fa-lock"></i>
                                Acesso ao sistema
                            </h3>
                        </div>
                        <div class="panel-body">

                            <?php
                            if (isset($_SESSION['output_message'])) {
                                print "<div class='alert alert-danger'>";
                                print("<p class='msgError'>" . $_SESSION['output_message'] . "</p>");
                                unset($_SESSION['output_message']);
                                print "</div>";
                            }
                            ?>
                            <form name="frm-login-cms" method="POST" action="<?= BASE_URL . "admin" ?>">                                
                                <div class="input-group">
                                    <div class="input-group-addon">Login:</div>
                                    <input class="form-control" id="login" type="text" name="login" value="<?= isset($_SESSION['form']['email']) ? $_SESSION['form']['email'] : null ?>" />
                                </div>

                                <div class="input-group">
                                    <div class="input-group-addon">Senha:</div>
                                    <input class="form-control" id="password" type="password" name="senha"/>
                                    <div class="input-group-addon">
                                        <button class="btn" type="submit" name="envia">
                                            <i class="fa fa-sign-in"></i>
                                            Acessar
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-3 col-lg-3"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p class="text-center">
                        Essa seção é parte integrante do web site <i><b>www.aggiltransportes.com.br</b></i>
                        <br/>Todos os direitos reservados.
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>