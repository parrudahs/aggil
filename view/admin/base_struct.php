<?php
$login = new Jcms\Core\Auth\Autenticacao();

if (isset($urls[2]) && $urls[2] == "sair") {
    if ($login->logoff())
        header('Location:' . BASE_URL . 'admin');
}

if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

ob_start();
?>

    <!DOCTYPE html>
    <html lang="pt-BR">
    <head>
        <title>:: Aggil Transportes - Transporte e Logística ::</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Google Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Baloo" rel="stylesheet"/>-->
        <!-- Google fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/font-awesome/css/font-awesome.css"/>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/bootstrap/css/bootstrap.css"/>
        <!-- BOOTSTRAP THEME -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/bootstrap/css/default.css"/>
        <!-- Data tables -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/data-tables/css/dataTables.bootstrap.css"/>
        <!-- Dropzone css -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/dropzone/dropzone.css"/>
        <!-- Font configuration -->
        <style>
            * {
                font-family: 'Roboto', sans-serif;
            }
        </style>
    </head>
    <body>
    <!-- MENU SUPERIOR -->
    <nav class="navbar navbar-default bd-color-primary">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs"
                        aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= BASE_URL ?>admin/inicial" title="Administração do site">
                    <i class="fa fa-3x fa-wrench font-color-primary"></i>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs">
                <ul class="nav navbar-nav">
                    <!-- <li><a href="#">Link</a></li> -->
                    <!-- OPÇÕES PARA DESTAQUES -->
                    <li>
                        <a href="<?= BASE_URL ?>admin/lista-destaques" title="Visualizar todos os destaques">
                            <i class="fa fa-2x fa-tv"></i>
                            DESTAQUE
                        </a>
                    </li>
                    <!--
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-tv"></i>
                            Destaques
                            <span class="caret gray"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-destaque">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar destaque
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-destaques">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar destaques
                                </a>
                            </li>
                        </ul>
                    </li>
                    -->
                    <!-- OPÇÕES PARA NOTÍCIAS -->
                    <li>
                        <a href="<?= BASE_URL ?>admin/lista-noticias" title="Visualizar todas as notícias">
                            <i class="fa fa-2x fa-newspaper-o"></i>
                            NOTÍCIA
                        </a>
                    </li>
                    <!--
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-newspaper-o"></i>
                            Notícias
                            <span class="caret gray"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-noticia">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar notícia
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-noticias">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar notícias
                                </a>
                            </li>
                        </ul>
                    </li>
                    -->
                    <!-- OPÇÕES PARA GALERIAS -->
                    <li>
                        <a href="<?= BASE_URL ?>admin/lista-galerias" title="Visualizar todas as galerias">
                            <i class="fa fa-2x fa-clone"></i>
                            GALERIA
                        </a>
                    </li>
                    <!--
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-photo"></i>
                            Galerias
                            <span class="caret gray"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-galeria">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar galeria
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-galerias">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar galerias
                                </a>
                            </li>
                        </ul>
                    </li>
                    -->
                    <li>
                        <a class="font-color-primary" href="<?= BASE_URL ?>admin/lista-institucionais">
                            <i class="fa fa-2x fa-institution"></i>
                            INSTITUCIONAL
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="text-danger" href="<?= BASE_URL ?>admin/inicial/sair">
                            <i class="fa fa-2x fa-sign-out" title="Fazer logout"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <br/>
    <br/>
    <!-- Content -->
    <?php include('content/' . $content_file); ?>
    <br/>
    <br/>
    <br/>
    <!-- Jquery -->
    <script src="<?= BASE_URL ?>/public/libs/bootstrap/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= BASE_URL ?>/public/libs/bootstrap/js/bootstrap.min.js"></script>
    <!-- Data tables -->
    <script src="<?= BASE_URL ?>/public/libs/data-tables/js/jquery.dataTables.js"></script>
    <script src="<?= BASE_URL ?>/public/libs/data-tables/js/dataTables.bootstrap.js"></script>

    <script src="<?= BASE_URL ?>/public/libs/ckeditor/ckeditor.js"></script>
    <script>
        $(document).ready(function () {

            var $$table = $('.datatable').DataTable({
                "dom": "<t>p"
            });
            $("#datatablessearch").on('keyup', function () {
                $$table.search(this.value).draw();
            });

            var $$editor = $('.ck-editor').each(function (key, el) {
                CKEDITOR.replace(el, {
                    toolbar: 'Basic',
                    uiColor: '#ffffff'
                });
            });

        });
    </script>

    </body>
    </html>

<?php
ob_flush();
?>