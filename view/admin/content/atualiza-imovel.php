<?php
    
    require_once("../addons/fckeditor/fckeditor.php");

    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null; 
    
    $imovel = new Imovel($id);	
    ImoveisDAO::setObject($imovel);
    ImoveisDAO::DBConnection();
    ImoveisDAO::getObjectDBData();    
    
    Forms::setFormName("frm-edit-imovel");
    Forms::getFormData();
    $nome = Forms::fieldValue("nome",$imovel->getNome());
    $codigo = Forms::fieldValue("codigo",$imovel->getCodigo());
    $categoria_id = Forms::fieldValue("categoria",$imovel->getCategoriaID());
    $imovel_tipo_id = Forms::fieldValue("imovel-tipo",$imovel->getImovelTipoID());
    $valor_imovel = Forms::fieldValue("valor-imovel",number_format($imovel->getValor(),"2",",","."));
    $quartos = Forms::fieldValue("quartos",$imovel->getQuartos());
    $suites = Forms::fieldValue("suites",$imovel->getSuites()); 
    $resumo = Forms::fieldValue("resumo",$imovel->getResumo());
    $descricao = Forms::fieldValue("descricao",$imovel->getDescricao());
    $cidade = Forms::fieldValue("cidade",$imovel->getCidade());
    $estado = Forms::fieldValue("estado",$imovel->getUf());
    $bairro = Forms::fieldValue("bairro",$imovel->getBairro());
    $destaque = Forms::fieldValue("destaque",$imovel->getDestaque());
    Forms::unsetFormData();
    
?>

<script type="text/javascript" src="js/jquery.maskMoney.js"></script>
<script>
    $(document).ready(
        function() {
            selSelect("combo-estados",'<?= $estado ?>');
            $(".money").maskMoney(
                {
                    thousands:'.',
                    decimal:',',
                    allowZero:true,
                    allowNegative:false,
                    defaultZero:true
                }
            );
            $('select[name="categoria"]').change(
                function() {
                    toggleVendaLocacao($(this).find(":selected").val());
                }
            )
            <?= ($categoria_id==1 OR $categoria_id==2) ? "toggleVendaLocacao($categoria_id);" : null ?>
        }
    );
</script>

<h1>Formul�rio para atualizar informa��es do Im�vel</h1>
<p class="cancel">
    <a href="lista-imoveis.php"><img src="images/voltar.png" title="cancelar" alt="cancelar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>			
<form id="frm-edit-imovel" name="frm-edit-imovel" method="POST" enctype="multipart/form-data" action="<?= DIR_SYS."/core/Controllers/Controllers.php?face=backend&object=imovel&action=atualizar&id=".$imovel->getImovelID() ?>">
    <table class="tb-form">
        <tbody>
            <tr>
                <th width="150px">C�digo</th>
                <td><input type="text" id="codigo" class="form-field" name="codigo" size="20" value="<?= $codigo ?>" /></td>
            </tr>            
            <tr>
                <th>Nome <span class="required">*</span></th>
                <td><input type="text" id="nome" class="form-field"  name="nome" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Tipo <span class="required">*</span></th>
                <td>
                    <select name="imovel-tipo" class="form-field">
                        <option value="0">- nenhum -</option>
                        <?php
                            
                            $imovelTipo = new ImovelTipo();
                            ImoveisTiposDAO::setObject($imovelTipo);
                            ImoveisTiposDAO::addSorter(array('nome','ASC'));
                            ImoveisTiposDAO::DBConnection();
                            
                            ImoveisTiposDAO::listItems(0,0);
    
                            $rowIndex = 0;
                            $numRows = $imovelTipo->getNumRows();
                            
                            while ($rowIndex < $numRows) {
                                ImoveisTiposDAO::fillObject();
                                ?>
                        <option value="<?= $imovelTipo->getImovelTipoID() ?>" <?= ($imovelTipo->getImovelTipoID() == $imovel_tipo_id) ? "selected" : null ?>><?= $imovelTipo->getNome() ?></option> 
                                <?php
                                $rowIndex++;
                            }                                        
                            
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Categoria <span class="required">*</span></th>
                <td>
                    <select name="categoria" class="form-field">
                        <option value="0">- nenhum -</option>
                        <?php
                            
                            $categoria = new Categoria();
                            CategoriasDAO::setObject($categoria);
                            CategoriasDAO::addSorter(array('nome','ASC'));
                            CategoriasDAO::DBConnection();
                            
                            CategoriasDAO::listItems(0,0);
    
                            $rowIndex = 0;
                            $numRows = $categoria->getNumRows();
                            
                            while ($rowIndex < $numRows) {
                                CategoriasDAO::fillObject();
                                ?>
                        <option value="<?= $categoria->getCategoriaID() ?>" <?= ($categoria->getCategoriaID() == $categoria_id) ? "selected" : null ?>><?= $categoria->getNome() ?></option> 
                                <?php
                                $rowIndex++;
                            }                                        
                            
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="locacao-venda">
                <th><span id="valor-imovel"></th>
                <td>R$&nbsp;<input type="text" class="form-field money" name="valor-imovel" size="10" value="<?= $valor_imovel ?>" /></td>
            </tr>
            <tr class="locacao-venda">
                <th>Quartos</th>
                <td><input type="text" class="form-field" name="quartos" size="4" value="<?= $quartos ?>" /></td>
            </tr>
            <tr class="locacao-venda">
                <th>Su�tes</th>
                <td><input type="text" class="form-field" name="suites" size="4" value="<?= $suites ?>" /></td>
            </tr>           
            <tr>
                <th>Resumo</th>
                <td>
                    <input type="text" id="resumo" class="form-field" name="resumo" size="60" value="<?= $resumo ?>" />
                </td>
            </tr>            
            <tr>
                <th>Descri��o</th>
                <td>
                    <?php
                        $oFCKeditor = new FCKeditor("descricao") ;
                        $oFCKeditor->BasePath = "../addons/fckeditor/";
                        $oFCKeditor->Height = 350;
                        $oFCKeditor->Value = $descricao;
                        $oFCKeditor->Create() ;
                    ?>
                </td>
            </tr>
            <tr>
                <th>Cidade / UF</th>
                <td>
                    <input type="text" id="cidade" class="form-field"  name="cidade" size="20" value="<?= $cidade ?>" />
                    <select name="estado" id="combo-estados" class="form-field">
                        <option value="0">- selecione -</option>
                        <option value="AC">Acre</option>
                        <option value="AL">Alagoas</option>
                        <option value="AP">Amap�</option>
                        <option value="AM">Amazonas</option>
                        <option value="BA">Bahia</option>
                        <option value="CE">Cear�</option>
                        <option value="DF">Distrito Federal</option>
                        <option value="ES">Espirito Santo</option>
                        <option value="GO">Goi�s</option>
                        <option value="MA">Maranh�o</option>
                        <option value="MT">Mato Grosso</option>
                        <option value="MS">Mato Grosso do Sul</option>
                        <option value="MG">Minas Gerais</option>
                        <option value="PA">Par�</option>
                        <option value="PB">Paraiba</option>
                        <option value="PR">Paran�</option>
                        <option value="PE">Pernambuco</option>
                        <option value="PI">Piau�</option>
                        <option value="RJ">Rio de Janeiro</option>
                        <option value="RN">Rio Grande do Norte</option>
                        <option value="RS">Rio Grande do Sul</option>
                        <option value="RO">Rond�nia</option>
                        <option value="RR">Roraima</option>
                        <option value="SC">Santa Catarina</option>
                        <option value="SP">S�o Paulo</option>
                        <option value="SE">Sergipe</option>
                        <option value="TO">Tocantis</option>
                    </select>                    
                </td>
            </tr>
            <tr>
                <th>Bairro</th>
                <td>
                    <input type="text" id="bairro" class="form-field"  name="bairro" size="30" value="<?= $bairro ?>" />
                </td>
            </tr>             
            <tr>
                <th>Foto principal</th>
                <td>
                    <?php

                        if (($imovel->getFotoPrincipal()) && is_file("../../imagens/imoveis/imovel".$imovel->getImovelID()."/".$imovel->getFotoPrincipal())) {
                            print("<img src=\"../../imagens/imoveis/imovel".$imovel->getImovelID()."/".$imovel->getFotoPrincipal()."\" width=\"200\" /><br/>");
                        }
                    
                    ?>
                    <input type="file" name="foto-principal" id="foto-principal" />
                    <input type="hidden" name="foto-atual" value="<?= $imovel->getFotoPrincipal() ?>" />
                    <br/><input type="checkbox" name="sem-imagem" value="1" />&nbsp;Sem imagem
                </td>
            </tr>           
            <tr>
                <th>Destaque</th>
                <td><input type="checkbox" name="destaque" id="destaque" value="1" <?= ($destaque==1) ? "checked" : null ?> /></td>
            </tr>                           
            <tr style="padding-bottom: 5px; padding-top: 5px;">
                <td colspan="2">Campos marcados com <span class="required">*</span> s�o de preenchimento obrigat�rio.</td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" class="button" onclick="javascript:submitForm('frm-edit-imovel');" value="Atualizar" /></td>
            </tr>                        
        </tbody>
    </table>
</form>