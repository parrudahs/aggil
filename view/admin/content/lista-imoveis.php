<?php

    $imovel = new Imovel();
    ImoveisDAO::DBConnection();
    ImoveisDAO::addSorter(array("nome","ASC"));
    ImoveisDAO::setObject($imovel);
    ImoveisDAO::listItems(0,0);
    
?>
<h1>Listagem dos Im�veis cadastrados</h1>
<p class="cancel">
    <a href="cadastro-imovel.php"><img src="images/novo.png" title="novo registro" alt="novo registro" border="0" /></a>
</p>            
<?php
    Forms::setFormName("frm-delete-imovel");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<fieldset class="legenda">
    <legend>Legenda</legend>
    <ul>
        <li><img src="images/icon-quartos-16x16.png" alt="Quartos" title="Quartos" border="0" />&nbsp;Cadastro de apartamentos/quartos/vagas/etc referentes ao im�vel</li>
        <li><img src="images/imagens_small.png" alt="Imagens" title="Imagens" border="0" />&nbsp;Gerencia imagens/fotos do Im�vel</li>
    </ul>
</fieldset>

<table class="tb-lista">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Destaque</th>
            <th width="40">&nbsp;</th>
            <th width="40">&nbsp;</th>
            <th width="40">&nbsp;</th>
            <th width="40">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $array_tipos=array();
            if ($imovel->getNumRows() > 0) {
                $rowIndex=0;
                $numRows = $imovel->getNumRows();
                while ($rowIndex < $numRows) {
                    ImoveisDAO::fillObject();
                    $tipo_label="";
                    if (isset($array_tipos[$imovel->getImovelTipoID()])) {
                        $tipo_label=$array_categorias[$imovel->getImovelTipoID()];
                    }
                    else {
                        $imovelTipo = new ImovelTipo();
                        $imovelTipo->setImovelTipoID($imovel->getImovelTipoID());
                        ImoveisTiposDAO::setObject($imovelTipo);
                        ImoveisTiposDAO::getObjectDBData();
                        $tipo_label=$imovelTipo->getNome();
                        $array_categorias[$imovel->getImovelTipoID()]=$imovelTipo->getNome();
                    }
        ?>
        <tr class="<?= ($rowIndex%2==0) ? "escuro" : "claro" ?>">
            <td><?= $imovel->getNome() ?></td>
            <td><?= $tipo_label ?></td>
            <td><?= ($imovel->getDestaque()) ? "<span class='sim'>sim</span>" : "<span class='nao'>n�o</span>" ?></td>
            <td><a href="locaveis-imovel.php?id=<?= $imovel->getImovelID() ?>"><img src="images/icon-quartos-16x16.png" alt="quartos" title="quartos" border="0" /></a</td>
            <td><a href="imagens-imovel.php?id=<?= $imovel->getImovelID() ?>"><img src="images/imagens_small.png" alt="imagens" title="imagens" border="0" /></a</td>
            <td><a href="atualiza-imovel.php?id=<?= $imovel->getImovelID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a</td>
            <td><a href="javascript:deleteRecord('<?= DIR_SYS ?>/core/controller/controller.php?face=backend&object=imovel&action=deletar&id=<?= $imovel->getImovelID() ?>');"><img src="images/delete_small.png" alt="deletar" title="deletar" border="0" /></a></td>
        </tr>
        <?php
                    $rowIndex++;	
                }
            }
            else
                print("<tr><td colspan='6'>- nenhum registro cadastrado -</td></tr>");
        ?>                  
    </tbody>
</table>