<?php
$noticia = new \Jcms\Core\Controllers\NoticiaController();
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-newspaper-o"></i>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/cadastro-noticia">
                    <i class="fa fa-2x fa-plus-circle"></i>
                    &nbsp;&nbsp;Nova notícia
                </a>
            </div>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h3 class="text-uppercase gray">Todos as notícias cadastradas</h3>
            <div class="row">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                } ?>
                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="datatablessearch" class="form-control" type="text"
                                   placeholder="Pesquisa na tabela"/>
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <table class="datatable table table-striped">
                <thead>
                <tr>
                    <th>Data</th>
                    <th>Título</th>
                    <th>Publicada</th>
                    <th>Destaque</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $n = $noticia->all();
                if ($noticia->getRowCount() > 0) {
                    foreach ($n as $noticias) {
                        ?>
                        <tr>
                            <td><?= date("d/m/Y H:i", $noticias['data_noticia']) ?></td>
                            <td><?= (strlen(html_entity_decode($noticias['titulo'])) > 80) ? substr(html_entity_decode($noticias['titulo']), 0, 80) : $noticias['titulo'] ?></td>
                            <td><?= ($noticias['publicada']) ? "<span class='sim'>sim</span>" : "<span class='nao'>não</span>" ?></td>
                            <td><?= ($noticias["destaque"]) ? "<span class='sim'>sim</span>" : "<span class='nao'>não</span>" ?></td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                                <a href="<?= BASE_URL ?>admin/atualiza-noticia/<?= $noticias['noticia_id'] ?>">
                                    <i class="fa fa-edit" title="Editar destaque."></i>
                                </a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="#"
                                   onclick="javascript: if (confirm('Você realmente deseja excluir o destaque?')) location.href='<?= BASE_URL ?>action/noticias/deletar/<?= $noticias['noticia_id'] ?>'">
                                    <i class="fa fa-trash-o text-danger" title="Deletar noticia."></i>
                                </a>
                            </td>
                        </tr>
                    <?php }
                } else
                    print("<tr><td colspan='7'>- nenhum registro cadastrado -</td></tr>");
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>