<?php 
$intituicao = new \Jcms\Core\Controllers\InstitucionalController();
$id = $urls[2];
$att = $intituicao->show($id);    
?>
<div class="page-header">
    <h3 class="text-uppercase text-info">
        Formulário para atualizar Conteúdo Institucional
        <small></small>
    </h3>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <form id="frm-add-destaque" name="frm-add-destaque" method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>action/institucionais/atualizar/<?= $att['conteudo_id'] ?>">
           <?php if (isset($_SESSION['output_message'])) { ?>
                <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                    <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                </div>
            <?php unset($_SESSION['output_message']); } ?>
            <div class="input-group">
                <div class="input-group-addon">Titulo</div>
                <input class="form-control" id="titulo" name="titulo" value="<?=  $att['titulo'] ?>"/>
                <div class="input-group-addon"><i class="text-danger">Obrigatório</i></div>
            </div>
            <div class="input-group">
                <div class="input-group-addon">Sub-titulo</div>
                <textarea type="text" class="form-control" id="sub_titulo" name="sub-titulo" cols="40" rows="2"><?=  $att['sub_titulo'] ?></textarea>
            </div>  
            <div class="form-group">
                <label for="resumo">Resumo</label>
                <textarea class="form-control ck-editor" id="conteudo" rows="3" name="chamada"><?= $att['chamada'] ?></textarea>
            </div>
            <div class="form-group">
                <label for="resumo">Conteúdo</label>
                <textarea class="form-control ck-editor" id="conteudo" name="conteudo"><?= $att['conteudo'] ?></textarea>
            </div>
            <div class="form-group">                
                <div class="row">
                    <div class="col-md-9">
                        <div class="input-group">
                           <div class="input-group-addon">Imagem</div>
                            <input class="form-control" id="imagem" type="file" name="imagem"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group-addon">Imagem Atual</div>
                        <img src="<?= BASE_URL."public/uploaded_files/institucional/".$att['imagem_destaque']  ?>" class="img-responsive">
                    </div>
                </div>
            </div>

            <input class="btn btn-default" type="submit" value="Atualizar" name="atualizar" />
        </form>
    </div>
</div>
