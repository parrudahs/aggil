<?php
$att = new Jcms\Core\Controllers\GaleriaController();
$id = $urls[2];
$att->show($id);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-clone"></i>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/cadastro-noticia">
                    <i class="fa fa-2x fa-plus-circle"></i>
                    &nbsp;&nbsp;Nova notícia
                </a>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/lista-galerias">
                    <i class="fa fa-2x fa-sort-alpha-asc"></i>
                    &nbsp;&nbsp;Listar galerias
                </a>
            </div>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h3 class="text-uppercase gray">Atualizar galeria</h3>
            <br/>
            <form method="POST" enctype="multipart/form-data"
                  action="<?= BASE_URL ?>action/galerias/atualizar/<?= $id ?>">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                } ?>
                <div class="input-group">
                    <div class="input-group-addon">Título</div>
                    <input class="form-control" id="titulo" name="titulo"
                           value="<?= isset($_SESSION['formulario_gal']['titulo']) ? $_SESSION['formulario_gal']['titulo'] : null ?>"/>
                    <div class="input-group-addon"><i class="text-danger">Obrigatório</i></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">Categoria</div>
                            <select class="form-control" name="categoria_id">
                                <option value="0">- nenhum -</option>
                                <?php foreach ($att->getCategorias() as $categoria) { ?>
                                    <option value="<?= $categoria['categoria_id'] ?>"><?= $categoria['nome'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-addon">Data e hora</div>
                    <input class="form-control mask-date-time" id="ordem" name="data_fotos"
                           value="<?= date("d/m/Y H:i") ?>"/>
                </div>
                <div class="form-group">
                    <label for="resumo">Descrição</label>
                    <textarea class="form-control ck-editor" id="descricao" name="descricao">
                    <?= isset($_SESSION['formulario_gal']['descricao']) ? $_SESSION['formulario_gal']['descricao'] : null ?>
                </textarea>
                </div>
                <div class="form-group">
                    <label for="resumo">Release</label>
                    <textarea class="form-control ck-editor" id="release" name="texto">
                    <?= isset($_SESSION['formulario_gal']['texto']) ? $_SESSION['formulario_gal']['texto'] : null ?>
                </textarea>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="destaque">Destaque:</label>
                        <br/>
                    </div>
                    <div class="col-md-10">
                        <input type="checkbox" name="destaque"
                               value="1" <?= (isset($_SESSION['formulario_gal']['destaque']) && $_SESSION['formulario_gal']['destaque'] == 1) ? "checked" : null ?> />
                        Destaque
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="publicado">Publicacão:</label>
                        <br/>
                    </div>
                    <div class="col-md-10">
                        <input type="checkbox" name="publicada"
                               value="1" <?= (isset($_SESSION['formulario_gal']['publicada']) && $_SESSION['formulario_gal']['publicada'] == 1) ? "checked" : null ?> />
                        Publicar
                        <br/>
                    </div>
                </div>
                <br/>
                <input class="btn btn-primary" type="submit" value="Atualizar" name="atualizar"/>
            </form>
            <?php unset($_SESSION['formulario_gal']); ?>
        </div>
    </div>
</div>