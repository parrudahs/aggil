<?php
$array_targets = array("NONE" => "Sem link", "NEW" => "Nova Janela", "SELF" => "Mesma Janela");
$att = new Jcms\Core\Controllers\DestaqueController();
$id = $urls[2];
$att->show($id);    
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-tv"></i>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/cadastro-destaque">
                    <i class="fa fa-2x fa-plus-circle"></i>
                    &nbsp;&nbsp;Novo destaque
                </a>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/lista-destaques">
                    <i class="fa fa-2x fa-sort-alpha-asc"></i>
                    &nbsp;&nbsp;Listar destaques
                </a>
            </div>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h3 class="text-uppercase gray">Atualizar destaque</h3>
            <br/>
            <form id="frm-add-destaque" name="frm-add-destaque" method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>action/destaques/atualizar/<?= $id ?>">

                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']); } ?>

                <div class="input-group">
                    <div class="input-group-addon">Título</div>
                    <input class="form-control" id="titulo" name="titulo" value="<?= isset($_SESSION['formulario_destaque']['titulo'])? $_SESSION['formulario_destaque']['titulo'] : null ?>"/>
                    <div class="input-group-addon"><i class="text-danger">Obrigatório</i></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">Ordem</div>
                            <input class="form-control" id="ordem" name="ordem" value="<?= isset($_SESSION['formulario_destaque']['ordem']) ? $_SESSION['formulario_destaque']['ordem'] : null ?>"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group">
                            <div class="input-group-addon">Imagem</div>
                            <input class="form-control" id="imagem" type="file" name="imagem"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="resumo">Descrição</label>
                    <textarea class="form-control ck-editor" id="conteudo" name="descricao">
                    <?= isset($_SESSION['formulario_destaque']['descricao'])? $_SESSION['formulario_destaque']['descricao'] : null ?>
                </textarea>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <label for="comportamento">Comportamento:</label>
                        <br/>
                    </div>
                    <div class="col-md-10">
                        <?php
                        foreach ($array_targets as $k => $t) {
                            ?>
                            <input type="radio" value="<?= $k ?>" name="comportamento"
                                <?= (isset($_SESSION['formulario_destaque']['comportamento']) && $_SESSION['formulario_destaque']['comportamento'] == $k )? 'checked' : null
                                ?>
                            />
                            <?= $t ?>
                            <br/>
                            <?php
                        }
                        ?>
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="publicado">Publicacão:</label>
                        <br/>
                    </div>
                    <div class="col-md-10">
                        <input type="checkbox" id="publicado" name="publicado" value="1" <?= (isset($_SESSION['formulario_destaque']['publicado']) && $_SESSION['formulario_destaque']['publicado'] == 1 ) ? 'checked' : null ?>/>
                        Publicar
                        <br/>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-addon">URL</div>
                    <input class="form-control" id="url" name="url" placeholder="Endereço da página que esse destaque irá abrir..." value="<?= isset($_SESSION['formulario_destaque']['url'])? $_SESSION['formulario_destaque']['url'] : null ?>"/>
                </div>
                <br/>
                <input class="btn btn-primary" type="submit" value="Atualizar" name="atualizar" />
            </form>
            <?php unset($_SESSION['formulario_destaque']); ?>
        </div>
    </div>
</div>