<?php
$galeria = new Jcms\Core\Controllers\GaleriaController;
$foto = new Jcms\Core\Controllers\FotoController;
$gal = $galeria->show($urls[2]);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-clone"></i>
                <br/>
                <br/>
                <a href="<?= BASE_URL ?>admin/atualiza-galeria/<?= $gal['galeria_id'] ?>">
                    <i class="fa fa-2x fa-edit"></i> Editar galeria
                </a>
                <br/>
                <br/>
                <a class="text-danger" href="#">
                    <i class="fa fa-2x fa-trash-o"></i> Remover galeria
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h2><?= $gal['titulo'] ?></h2>
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <?php if (isset($_SESSION['output_message'])) { ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']);
                    } ?>
                    <!-- Bloco para upload de imagens com dropzone -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h4><i class="fa fa-upload"></i> Upload de imagens:</h4>
                            <form class="dropzone" action="<?= BASE_URL ?>action/galerias/adicionar-fotos/<?= $gal['galeria_id'] ?>" class="" id="my-awesome-dropzone"></form>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <!-- Exibição de todas as fotos da galeria -->
                    <?php
                    $fotos = $foto->getFotosGaleria($urls[2]);
                    if ($foto->getRowCount() > 0) {
                        foreach ($fotos as $f) {
                            ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                    <div class="text-center">
                                        <!-- Imagem -->
                                        <img src="<?= BASE_URL ?>public/uploaded_files/galerias/200x200-<?= $f['arquivo'] ?>"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-8">
                                    <form action="<?= BASE_URL ?>action/galerias/atualizar-fotos/<?= $urls[2] ?>/<?= $f['foto_id'] ?>">
                                        <div class="input-group">
                                            <div class="input-group-addon">Credito:</div>
                                            <input class="form-control" type="text"/>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon">Descrição:</div>
                                            <input class="form-control" type="text"/>
                                        </div>
                                        <br/>
                                        <button class="btn btn-default" type="submit">
                                            <i class="fa fa-2x fa-edit text-primary"></i>
                                            Atualizar
                                        </button>
                                        <button class="btn btn-default" type="submit">
                                            <i class="fa fa-2x fa-trash-o text-danger"></i>
                                            Remover
                                        </button>
                                    </form>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <br/>
                                    <br/>
                                    <hr/>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <h3>Essa galeria não tem nenhuma foto.</h3>
                        <?php
                    }
                    ?>
                    <b class="text-uppercase">Data das
                        fotos:</b> <?= $gal['data_fotos'] ? date("d/m/Y", $gal['data_fotos']) : "-" ?>
                    <br/>
                    <b class="text-uppercase">Data do cadastro</b> <?= date("d/m/Y H:i", $gal['timestamp']) ?>
                    <?php unset($_SESSION['formulario_gal']); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= BASE_URL ?>/public/libs/dropzone/dropzone.js"></script>
<script>
    Dropzone.options.myAwesomeDropzone = {
        addRemoveLinks: false,
        dictDefaultMessage: "Arraste e solte a foto aqui!",
        init: function () {
            this.on("queuecomplete", function (file) {
                location.reload();
            });
        }
    };
</script>