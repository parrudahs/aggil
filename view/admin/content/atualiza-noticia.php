<?php
$att = new Jcms\Core\Controllers\NoticiaController();
$id = $urls[2];
$att->show($id);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-tv"></i>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/cadastro-noticia">
                    <i class="fa fa-2x fa-plus-circle"></i>
                    &nbsp;&nbsp;Nova notícia
                </a>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/lista-noticias">
                    <i class="fa fa-2x fa-sort-alpha-asc"></i>
                    &nbsp;&nbsp;Listar notícias
                </a>
            </div>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h3 class="text-uppercase gray">Atualizar notícia</h3>
            <br/>
            <form method="POST" enctype="multipart/form-data"
                  action="<?= BASE_URL ?>action/noticias/atualizar/<?= $id ?>">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                } ?>
                <div class="input-group">
                    <div class="input-group-addon">Título</div>
                    <input class="form-control" id="titulo" name="titulo"
                           value="<?= isset($_SESSION['formulario_noticia']['titulo']) ? $_SESSION['formulario_noticia']['titulo'] : null ?>"/>
                    <div class="input-group-addon"><i class="text-danger">Obrigatório</i></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">Categoria</div>
                            <select class="form-control" name="categoria_id">
                                <option value="0">- nenhum -</option>
                                <?php foreach ($att->getCategorias() as $categoria) { ?>
                                    <option value="<?= $categoria['categoria_id'] ?>"<?= (isset($_SESSION['formulario_noticia']['categoria_id']) && $_SESSION['formulario_noticia']['categoria_id'] == $categoria['categoria_id']) ? ' selected' : null ?> > <?= $categoria['nome'] ?></option>
                                <?php } ?></select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="resumo">Resumo</label>
                    <textarea class="form-control ck-editor" id="conteudo" name="resumo">
                    <?= isset($_SESSION['formulario_noticia']['resumo']) ? $_SESSION['formulario_noticia']['resumo'] : null ?>
                </textarea>
                </div>
                <div class="form-group">
                    <label for="resumo">Conteudo</label>
                    <textarea class="form-control ck-editor" id="conteudo" name="conteudo">
                    <?= isset($_SESSION['formulario_noticia']['conteudo']) ? $_SESSION['formulario_noticia']['conteudo'] : null ?>
                </textarea>
                </div>
                <div class="input-group">
                    <div class="input-group-addon">Data e hora</div>
                    <input class="form-control mask-date-time" id="ordem" name="data_noticia"
                           value="<?= date("d/m/Y H:i") ?>"/>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="input-group">
                                <div class="input-group-addon">Imagem</div>
                                <input class="form-control" id="imagem" type="file" name="imagem"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group-addon">Imagem Atual</div>
                            <img src="<?= BASE_URL . "public/uploaded_files/noticias/400x300-" . $_SESSION['formulario_noticia']['imagem'] ?>"
                                 class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="publicado">Destaque:</label>
                        <br/>
                    </div>
                    <div class="col-md-10">
                        <input type="checkbox" id="publicado" name="destaque"
                               value="1" <?= (isset($_SESSION['formulario_noticia']['destaque']) && $_SESSION['formulario_noticia']['destaque'] == 1) ? "checked" : null ?> />
                        Destaque
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="publicado">Publicacão:</label>
                        <br/>
                    </div>
                    <div class="col-md-10">
                        <input type="checkbox" id="publicado" name="publicada"
                               value="1" <?= (isset($_SESSION['formulario_noticia']['publicada']) && $_SESSION['formulario_noticia']['publicada'] == 1) ? "checked" : null ?> />
                        Publicar
                        <br/>
                    </div>
                </div>
                <br/>
                <input class="btn btn-primary" type="submit" value="Atualizar" name="atualizar"/>
            </form>
            <?php unset($_SESSION['formulario_noticia']); ?>
        </div>
    </div>
</div>