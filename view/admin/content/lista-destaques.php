<?php
$destaque = new Jcms\Core\Controllers\DestaqueController();
$d = $destaque->listAdmin();


?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-tv"></i>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/cadastro-destaque">
                    <i class="fa fa-2x fa-plus-circle"></i>
                    &nbsp;&nbsp;Novo destaque
                </a>
            </div>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h3 class="text-uppercase gray">Todos os destaques cadastrados</h3>
            <div class="row">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                } ?>
                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="datatablessearch" class="form-control" type="text"
                                   placeholder="Pesquisa na tabela"/>
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <table class="datatable table table-striped">
                <thead>
                <tr>
                    <th>CADASTRO</th>
                    <th>TÍTULO</th>
                    <th>ORDEM</th>
                    <th>FOI PUBLICADO?</th>
                    <th>AÇÕES</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($destaque->getRowCount() > 0) {
                    foreach ($d as $dest) {
                        ?>
                        <tr>
                            <td><?= isset($dest['timestamp_cadastro']) ? date("d/m/Y H:i", $dest['timestamp_cadastro']) : null ?></td>
                            <td><?= isset($dest['titulo']) ? $dest['titulo'] : null ?></td>
                            <td><?= isset($dest['ordem']) ? $dest['ordem'] : null ?></td>
                            <td><?= isset($dest['publicado']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-2x fa-close text-danger'></i>" ?></td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                                <a href="<?= BASE_URL ?>admin/atualiza-destaque/<?= $dest['destaque_id'] ?>">
                                    <i class="fa fa-edit" title="Editar destaque."></i>
                                </a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="#"
                                   onclick="javascript: if (confirm('Você realmente deseja excluir o destaque?'))location.href='<?= BASE_URL ?>action/destaques/deletar/<?= $dest['destaque_id'] ?>'">
                                    <i class="fa fa-trash-o text-danger" title="Deletar destaque."></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    print("<tr><td colspan='6'>Nenhum registro...</td></tr>");
                }
                ?>
                </tbody>
            </table>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Confirmação</h4>
                        </div>
                        <div class="modal-body">
                            Mensagem de confirmação
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>