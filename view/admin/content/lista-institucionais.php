<?php
    $institucional = new \Jcms\Core\Controllers\InstitucionalController();
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3 class="text-uppercase text-info">
                Listagem dos Conteúdos Institucionais
                <small></small>
            </h3>
        </div>
    </div>
</div>

<div class="row">

    <?php if (isset($_SESSION['output_message'])) { ?>
        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
        </div>
    <?php unset($_SESSION['output_message']); } ?>
    <div class="col-md-12">
        <form>
            <div class="input-group">
                <input id="datatablessearch" class="form-control" type="text"  placeholder="Pesquisa na tabela"/>
                <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                </div>
            </div>
        </form>
    </div>
</div>

<table class="datatable table table-striped">
    <thead>
        <tr>
            <th>Título</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $dados = $institucional->all();
        if ($institucional->getRowCount() > 0) {
            foreach ($dados as $ins) {    ?>
        <tr>
            <td><?= $ins['titulo'] ?></td>
            <td width="20%">
                <a href="<?= BASE_URL ?>admin/atualiza-institucional/<?= $ins['conteudo_id'] ?>">
                    <i class="fa fa-edit" title="Editar destaque."></i>
                </a>
            </td>
        </tr>
                <?php
            }
        }
        else
            print("<tr><td colspan=\"2\">- nenhum registro cadastrado.</td></tr>");                    
    ?>                    
    </tbody>
</table>