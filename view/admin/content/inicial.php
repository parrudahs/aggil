<?php
$config = new \Jcms\Core\Controllers\ConfiguracaoController();
if (isset($_POST['atualizar'])) {
    $config->updateConfiguracao($_POST);
}

?>


<!--
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="#"><i class="fa fa-2x fa-envelope-o"></i>&nbsp;&nbsp;E-mail</a>
                </li>
            </ul>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
        </div>
    </div>
</div>
-->


<!-- Content -->
<div class="container-fluid">
    <div class="row">
        <!--Left -->
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <!--Middle -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="#"><i class="fa fa-2x fa-envelope-o"></i>&nbsp;&nbsp;E-mail</a>
                </li>
            </ul>
            <br/>
            <br/>
        </div>
        <!--Right -->
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">

            <div class="alert alert-success">
                <i class="fa fa-2x fa-user-o"></i>
                &nbsp;Olá <strong><?= $_SESSION['instances']['backend']['user'] ?></strong>, Seja bem-vindo(a) ao
                Sistema de
                Gerenciamento de Conteúdo do <i>web site</i> <strong>Aggil Transportes</strong>
            </div>

            <?php
            if (isset($_SESSION['output_message'])) {
                print "<div class='alert alert-" . $_SESSION['output_message_tipo'] . "'>";
                print("<p class='msgError'>" . $_SESSION['output_message'] . "</p>");
                unset($_SESSION['output_message']);
                print "</div>";
            }
            ?>

            <div class="page-header">
                <h2>
                    <i class="fa fa-cog"></i>
                    CONFIGURAÇÕES
                    <small></small>
                </h2>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                    <form name="frm-configs" method="POST" action="<?= BASE_URL ?>admin/inicial">
                        <?php $configs = $config->indexPainel(); ?>
                        <label for="senha">Alterar a senha do sistema:</label>
                        <div class="input-group">
                            <div class="input-group-addon">Nova senha:</div>
                            <input class="form-control" type="password" name="senha"/>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon">Confirmar senha:</div>
                            <input class="form-control" type="password" name="re-senha"/>
                        </div>

                        <label for="senha">Alterar o Email do sistema:</label>
                        <div class="input-group">
                            <div class="input-group-addon">Email para receber contato:</div>
                            <textarea class="form-control" name="emails"><?= $configs['EMAILS_CONTATO'] ?></textarea>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Atualizar configurações" name="atualizar"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
