<?php

    $categoria_pai = new Categoria();
    CategoriasDAO::DBConnection();
    CategoriasDAO::setObject($categoria_pai);
    CategoriasDAO::addSorter(array("ordem","ASC"));
    
    $page = isset($_REQUEST['page']) ? (int) $_REQUEST['page'] : 1;
    
    $rows_page = 30;
    
    CategoriasDAO::listItems((($page-1)*$rows_page),$rows_page);    
    
    $paginacao = new Paginacao();

    $paginacao->total_resultados=$categoria_pai->getNumRowsWL();
    $paginacao->linhas_por_pagina=$rows_page;
    $paginacao->pagina_atual=$page;
    $paginacao->limiar=5;
    $paginacao->max_paginas=10;
    $paginacao->base_url=$_SERVER['PHP_SELF'];
    $paginacao->query_string=$_SERVER['QUERY_STRING'];
    $paginacao->paginar();

    $array_categorias = ListasUtil::listaCategorias();

?>

<h1>Listagem das Categorias cadastradas</h1>
<p class="cancel">
    <a href="cadastro-categoria.php"><img src="images/novo.png" title="novo registro" alt="novo registro" border="0" /></a>
</p>
<?php
    Forms::setFormName("frm-delete-categoria");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<div id="paginacao">
    Pagina��o:<br/>
    <?= $paginacao->getPaginacao() ?>
</div>
<table class="tb-lista">
    <thead>
        <tr>
            <th>Categoria</th>
            <th>Categoria pai</th>
            <th width="40">&nbsp;</th>
            <th width="40">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <?php                    
        if ($categoria_pai->getNumRows() > 0) {
            
            $class_row=true;
            
            $categoria_pai = new Categoria();
            CategoriasDAO::setObject($categoria_pai);
            CategoriasDAO::addFilter(array('categoria_pai_id','=','0'));
            CategoriasDAO::addSorter(array('ordem','ASC'));
            CategoriasDAO::DBConnection();
            
            CategoriasDAO::listItems(0,0);

            $rowIndex = 0;
            $numRows = $categoria_pai->getNumRows();
            
            while ($rowIndex < $numRows) {
                CategoriasDAO::fillObject();
                
                ?>
        <tr class="<?= ($class_row) ? "escuro" : "claro" ?>">
            <td><?= $categoria_pai->getNome() ?></td>
            <td><?= ($categoria_pai->getCategoriaPaiID()) ? $array_categorias[$categoria->getCategoriaPaiID()] : "-" ?>
            <td><a href="atualiza-categoria.php?id=<?= $categoria_pai->getCategoriaID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a</td>
            <td><a href="javascript:deleteRecord('<?= DIR_SYS ?>/core/controller/controller.php?face=backend&object=categoria&action=deletar&id=<?= $categoria_pai->getCategoriaID() ?>');"><img src="images/delete_small.png" alt="deletar" title="deletar" border="0" /></a></td>
        </tr>
                <?php                                
                
                $class_row=!$class_row;
                
                $categoria_filha = new Categoria();
                CategoriasDAO::setObject($categoria_filha);
                CategoriasDAO::clearFilter();
                CategoriasDAO::addFilter(array('categoria_pai_id','=',$categoria_pai->getCategoriaID()));
                CategoriasDAO::clearSorter();
                CategoriasDAO::addSorter(array('ordem','ASC'));
                CategoriasDAO::listItems(0,0);

                $rowIndexFilha = 0;
                $numRowsFilhas = $categoria_filha->getNumRows();
            
                while ($rowIndexFilha < $numRowsFilhas) {
                    CategoriasDAO::fillObject();
                ?>
        <tr class="<?= ($class_row) ? "escuro" : "claro" ?>">
            <td><?= $categoria_pai->getNome()."	&#187; ".$categoria_filha->getNome() ?></td>
            <td><?= ($categoria_filha->getCategoriaPaiID()) ? $array_categorias[$categoria_filha->getCategoriaPaiID()] : "-" ?>
            <td><a href="atualiza-categoria.php?id=<?= $categoria_filha->getCategoriaID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a</td>
            <td><a href="javascript:deleteRecord('<?= DIR_SYS ?>/core/controller/controller.php?face=backend&object=categoria&action=deletar&id=<?= $categoria_filha->getCategoriaID() ?>');"><img src="images/delete_small.png" alt="deletar" title="deletar" border="0" /></a></td>
        </tr>
                <?php
                    $class_row=!$class_row;
                    $rowIndexFilha++;
                }
                
                CategoriasDAO::setObject($categoria_pai);
                
                $rowIndex++;
            }
                }
                else
                    print("<tr><td colspan='4'>- nenhum registro cadastrado -</td></tr>");
        ?>                  
    </tbody>
</table>