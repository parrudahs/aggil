<?php

    require_once("../addons/fckeditor/fckeditor.php");

    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null; 
    
    $categoria = new Categoria($id);	
    CategoriasDAO::setObject($categoria);
    CategoriasDAO::DBConnection();
    CategoriasDAO::getObjectDBData();

    Forms::setFormName("frm-edit-categoria");
    Forms::getFormData();
    $categoria_pai = Forms::fieldValue("categoria-pai",$categoria->getCategoriaPaiID());
    $nome = Forms::fieldValue("nome",$categoria->getNome());
    $descricao = Forms::fieldValue("descricao",$categoria->getDescricao());
    $ordem = Forms::fieldValue("ordem",$categoria->getOrdem());
    Forms::unsetFormData();
    
?>

<h1>Formul�rio para atualizar Categoria</h1>
<p class="cancel">
    <a href="lista-categorias.php"><img src="images/voltar.png" title="voltar" alt="voltar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>			
<form id="frm-edit-categoria" name="frm-edit-categoria" method="POST" enctype="multipart/form-data" action="<?= DIR_SYS."/core/Controllers/Controllers.php?face=backend&object=categoria&action=atualizar&id=".$categoria->getCategoriaID() ?>">
    <table class="tb-form">
        <tbody>
             <tr>
                <th width="120">Categoria pai</th>
                <td>
                    <select name="categoria-pai">
                        <option value="0">- nenhuma -</option>
                        <?php
                        
                            $categoria = new Categoria();
                            CategoriasDAO::setObject($categoria);
                            CategoriasDAO::addFilter(array("categoria_id","!=",$categoria->getCategoriaID()));
                            CategoriasDAO::addFilter(array("categoria_pai_id","=","0"));
                            CategoriasDAO::addSorter(array("nome","ASC"));
                            CategoriasDAO::DBConnection();
                            CategoriasDAO::listItems(0,0);
                        
                            $rowIndex=0;
                            $numRows=$categoria->getNumRows();
                            
                            while ($rowIndex<$numRows) {
                                CategoriasDAO::fillObject();
                                ?>
                        <option value="<?= $categoria->getCategoriaID() ?>" <?= ($categoria_pai==$categoria->getCategoriaID()) ? "selected" : null ?>><?= $categoria->getNome() ?></option>
                                <?php
                                $rowIndex++;
                            }
                            
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Nome *</th>
                <td><input type="text" name="nome" id="nome" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Descri��o</th>
                <td>
                    <?php
                        $oFCKeditor = new FCKeditor("descricao") ;
                        $oFCKeditor->BasePath = "../addons/fckeditor/";
                        $oFCKeditor->Height = 250;
                        $oFCKeditor->Value = $descricao;
                        $oFCKeditor->Create() ;
                    ?>
                </td>
            </tr>
            <tr>
                <th>Ordem</th>
                <td><input type="text" name="ordem" id="ordem" size="10" value="<?= $ordem ?>" /></td>
            </tr>            
            <tr>
                <td colspan="2">Campos marcados com * s�o de preenchimento obrigat�rio.</td>
            </tr>                            
            <tr>
                <td colspan="2">
                    <input type="button" class="button" onclick="javascript:submitForm('frm-edit-categoria');" value="Atualizar" />
                </td>
            </tr>
        </tbody>
    </table>

</form>