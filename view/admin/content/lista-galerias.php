<?php
$gal = new Jcms\Core\Controllers\GaleriaController;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 hidden-xs hidden-sm"></div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
            <div class="text-center">
                <i class="fa fa-5x fa-clone"></i>
                <br/>
                <br/>
                <a class="blue" href="<?= BASE_URL ?>admin/cadastro-galeria">
                    <i class="fa fa-2x fa-plus-circle"></i>
                    &nbsp;&nbsp;Nova galeria
                </a>
            </div>
            <br/>
            <br/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
            <h3 class="text-uppercase gray">Todos as notícias cadastradas</h3>
            <div class="row">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                    unset($_SESSION['formulario_gal']);
                }
                ?>

                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="datatablessearch" class="form-control" type="text"
                                   placeholder="Pesquisa na tabela"/>
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php if (isset($_SESSION['output_message'])) { ?>
                <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                    <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                </div>
                <?php unset($_SESSION['output_message']);
            }
            ?>
            <table class="datatable table table-striped">
                <thead>
                <tr>
                    <th>DATA DAS FOTOS</th>
                    <th>TÍTULO</th>
                    <th>FOI PUBLICADO?</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($gal->todasAsGalerias() as $dest) {
                    ?>
                    <tr>
                        <td><?= date('d/m/Y H:m:s', $dest['data_fotos']) ?></td>
                        <td><?= $dest['titulo'] ?></td>
                        <td><?= ($dest['publicada']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-2x fa-close text-danger'></i>" ?></td>
                        <td>
                            <a href="<?= BASE_URL ?>admin/gerencia-fotos/<?= $dest['galeria_id'] ?>"
                               title="Gerenciar imagens.">
                                <i class="fa fa-1p4x fa-cog"></i>
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="<?= BASE_URL ?>admin/atualiza-galeria/<?= $dest['galeria_id'] ?>"
                               title="Editar destaque.">
                                <i class="fa fa-edit"></i>
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#"
                               onclick="javascript: if (confirm('Você realmente deseja excluir a galeria?')) location.href = '<?= BASE_URL ?>action/galerias/deletar/<?= $dest['galeria_id'] ?>'">
                                <i class="fa fa-trash-o text-danger" title="Deletar destaque."></i>
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>