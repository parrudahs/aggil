<?php

    require_once('Jcms/setup/autoload.php');

    session_start();
    
    //error_reporting(0);
    
    
    $field_sec_name = Forms::getFormToken("frm-envia-contato");
    
    $success=false;
    if (isset($_REQUEST[$field_sec_name]) && ($_REQUEST[$field_sec_name]==1)) {
        $nome=$_REQUEST['name'];
        $email=$_REQUEST['email'];
        $telefone=$_REQUEST['telefone'];
        $mensagem=$_REQUEST['message'];
        
        if ($nome && $email && $telefone && $mensagem) {
            if (preg_match("/^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]+(.){1}[a-zA-Z]{2,4})+$/",$email)) {
                
                $config_emails = new Configuracao();
                $config_emails->setName('EMAILS_CONTATO');
                ConfiguracoesDAO::setObject($config_emails);
                ConfiguracoesDAO::DBConnection();
                ConfiguracoesDAO::getObjectDBData();
                $emails_contato = $config_emails->getValue();                
                
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "From: Aggil Transportes <contato@aggiltransportes.com.br>\r\n";
                $headers .= "Reply-To: ".$nome." <$email>";
                $email_to=$emails_contato;
                $subject="[CONTATO] ".$nome;
                $email_body="<div style='font-face: verdana;font-size 14px;'>
                                <p><strong>Nome:</strong>&nbsp;$nome</p>
                                <p><strong>E-mail:</strong>&nbsp;$email</p>
                                <p><strong>Telefone:</strong>&nbsp;$telefone</p>
                                <p><strong>Mensagem</strong><br />".$mensagem."</p>
                            </div>";
                if (mail($email_to,$subject,$email_body,$headers)) {
                    $success=true;
                    Forms::unsetFormToken("frm-envia-contato");
                    $formToken = Forms::setFormToken("frm-envia-contato");
                }
                else {
                    $msg="Erro t�cnico enviando contato!<br />Entre em contato com o administrador do site ou tente novamente mais tarde.";
                }
            }
            else {
                $msg="O e-mail informado parece inv�lido! � necess�rio um e-mail valido para respondermos o seu contato.";
            }
        }
        else {
            $msg="� necess�rio o preenchimento de todos os campos!";
        }
    }
    else {
        $msg="Formu�rio inv�lido";
    }
    
    header('Content-Type: application/json');
    
    $json['status'] = $success ? "ok" : "error";
    $json['message'] = utf8_encode($msg);
    $json['token'] = $formToken;
    
    echo json_encode($json);

?>