<ul>
    <li class="active"><a href="<?= BASE_URL ?>/index.php">Home</a></li>
    <li><a href="<?= BASE_URL ?>/empresa">Empresa</a></li>
    <li><a href="<?= BASE_URL ?>/noticias">Notícias</a></li>
    <li>
        <a href="#">Ferramentas<span></span></a>
        <ul>
            <li><a href="<?= BASE_URL ?>/coleta">Coleta</a></li>
            <li><a href="<?= BASE_URL ?>/rastreio">Rastreio</a></li>
            <li><a href="https://mail.google.com/a/aggiltransportes.com.br" target="_blank">Webmail</a></li>
        </ul>
    </li>
    <li><a href="<?= BASE_URL ?>/galerias">Galeria de Fotos</a></li>
    <li><a href="<?= BASE_URL ?>/contato">Contato</a></li>
</ul>