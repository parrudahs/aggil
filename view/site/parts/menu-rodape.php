<ul>
    <li <?= $requested_url=="" ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>index.php">Home</a></li>
    <li <?= $requested_url=="empresa" ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>empresa">Empresa</a></li>
    <li <?= ($requested_url=="lista-noticias" || $requested_url=="ler-noticia") ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>noticias">Notícias</a></li>
    <li <?= ($requested_url=="lista-galerias" || $requested_url=="ver-galeria") ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>galerias">Galeria de Fotos</a></li>
    <li <?= $requested_url=="contato" ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>contato">Contato</a></li>
</ul>