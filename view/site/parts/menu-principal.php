<ul>
    <li <?= $requested_url=="" ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>index.php">Home</a></li>
    <li <?= $requested_url=="empresa" ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>empresa">Empresa</a></li>
    <li <?= ($requested_url=="lista-noticias" || $requested_url=="ler-noticia") ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>noticias">Notícias</a></li>
    <li <?= ($requested_url=="coleta" || $requested_url=="rastreio") ? 'class="active"' : null ?>>
        <a href="#">Ferramentas<span></span></a>
        <ul>
            <li><a href="<?= BASE_URL ?>coleta">Coleta</a></li>
            <li><a href="<?= BASE_URL ?>rastreio">Rastreio</a></li>
            <li><a href="https://mail.google.com/a/aggiltransportes.com.br" target="_blank">Webmail</a></li>
        </ul>
    </li>
    <li <?= ($requested_url=="lista-galerias" || $requested_url=="ver-galeria") ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>galerias">Galeria de Fotos</a></li>
    <li <?= $requested_url=="contato" ? 'class="active"' : null ?>><a href="<?= BASE_URL ?>contato">Contato</a></li>
</ul>