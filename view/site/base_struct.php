﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="iso-8859-1" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Aggil - Transportes e Logística</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= BASE_URL ?>public/css/images/favicon.ico" />
        <link rel="stylesheet" href="<?= BASE_URL ?>public/css/style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE_URL ?>public/css/flexslider-1.8.css" type="text/css" media="all" />
        <link media="screen" rel="stylesheet" href="<?= BASE_URL ?>public/css/colorbox.css" />
        <script src="<?= BASE_URL ?>public/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="<?= BASE_URL ?>public/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
        <script src="<?= BASE_URL ?>public/js/jquery.maskedinput.min.js"></script>
        <!--[if lt IE 9]>
                <script src="<?= BASE_URL ?>public/js/modernizr.custom.js"></script>
        <![endif]-->
        <script src="<?= BASE_URL ?>public/js/jquery.flexslider-1.8.min.js" type="text/javascript"></script>
        <script src="<?= BASE_URL ?>public/js/jquery.colorbox.js"></script>
        <script src="<?= BASE_URL ?>public/js/functions.js" type="text/javascript"></script>
    </head>
    <body>
        <!-- wrapper -->
        <div id="wrapper">
            <!-- shell -->
            <div class="shell">
                <!-- container -->
                <div class="container">
                    <!-- header -->
                    <header class="header">
                        <h1 id="logo"><a href="<?= BASE_URL ?>index.php">Logo</a></h1>
                        <nav id="navigation">
                            <?php
                                include("parts/menu-principal.php");
                            ?>
                        </nav>
                        <div class="cl">&nbsp;</div>
                    </header>
                    <!-- end of header -->
                    <div class="main">
                        <!-- slider -->
                        <div class="flexslider">
                            <ul class="slides">
                                <?php
                                    $d = new Jcms\Core\Controllers\DestaqueController();
                                    foreach($d->index() as $destaque){
                                ?>
                                <li>
                                    <img src="<?= BASE_URL ?>public/uploaded_files/destaque/<?= $destaque['imagem'] ?>" />
                                    <?php
                                        if ($destaque['comportamento'] !="NONE" && $destaque['url']!="") {
                                    ?>
                                    <a href="<?= $destaque['url'] ?>" <?= ($destaque['comportamento']=="NEW") ? 'target="_blank"' : null ?>>
                                    <?php
                                        }
                                    ?>
                                        <div class="slide-cnt">
                                            <h2><?= $destaque['titulo'] ?></h2>
                                            <?= $destaque['descricao']  ?>
                                        </div>
                                    <?php
                                        if ($destaque['comportamento'] !="NONE" && $destaque['url']!="") {
                                    ?>
                                    </a>                                 
                                </li>                    
                                <?php
                                        }
                                    }                                    
                                ?>                               
                            </ul>
                            <div class="slider-holder"></div>
                        </div>
                        <!-- end of slider -->
                        <?php
                            include("content/".$requested_file);
                        ?>
                        <!-- services -->
                        <section class="services">
                            <div class="widget">
                                <h3>Aggil Transportes LTDA - ME</h3>
                                <ul>
									<li>Rua Bela Vista do Paraiso, 1782</li>
									<li>Jardim Nova Portugal</li>
									<li>CEP 07171-000 - Garulhos – SP</li>
									<li>Telefone: (11) 4016-3521 (DISCAR DDD)</li>
									<li>Whatsapp: (11) 96831-2093 (SOMENTE WHATSAPP)</li>
									<li>E-mail: <a href="mailto:edson@aggiltransportes.com.br?subject=Contato-Site">edson@aggiltransportes.com.br</a></li>
                                </ul>
                            </div>
                            <div class="widget socials-widget">
                                <?php
                                    include("parts/social.php");
                                ?>
                            </div>
                            <div class="cl">&nbsp;</div>
                        </section>
                        <!-- end of services -->
                    </div>
                    <!-- end of main -->
                </div>
                <!-- end of container -->	
                <div class="footer">
                    <nav class="footer-nav">
                        <?php
                            require("parts/menu-rodape.php");
                        ?>
                    </nav>
                    <p class="copy">Aggil Transportes e Logística - Copyright &copy; 2014 - Todos os direitos reservados.</p>
                </div>
            </div>
            <!-- end of shell -->
        </div>
        <!-- end of wrappert -->
    </body>
</html>