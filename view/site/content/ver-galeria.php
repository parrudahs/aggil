<?php

    $arr = preg_split ("|/|",$_GET['id']);
    
    $galeria_id=(int) $arr[0];
    
    $galeria = new Galeria($galeria_id);
    GaleriasDAO::setObject($galeria);
    GaleriasDAO::DBConnection();
    GaleriasDAO::getObjectDBData();

?>
<section class="cols">
    <h2>Galeria de fotos</h2>
    <?php
        if ($galeria->getNumRows()>0) {
    ?>
    <h3><?= $galeria->getTitulo() ?></h3>
    <p style="text-align: justify;">
        <?= $galeria->getDescricao() ?>
    </p>
    <table id="tb-fotos-galeria">
        <tbody>
            <?php
                
                $foto = new Foto();
                FotosDAO::setObject($foto);
                FotosDAO::addFilter(array("galeria_id","=",$galeria->getGaleriaID()));
                FotosDAO::listItems(0,0);                                
                
                $rowIndex=0;
                $numRows=$foto->getNumRows();
                
                $dir_fotos = "galeria-".$galeria->getGaleriaID()."-".date("dmY",$galeria->getTimestamp());
                
                while ($rowIndex < $numRows) {
                    FotosDAO::fillObject();
                    $src = BASE_URL."imagens/galerias/".$dir_fotos."/thumbs/".$foto->getArquivo();
                    $link = BASE_URL."imagens/galerias/".$dir_fotos."/".$foto->getArquivo();
                    if ($rowIndex%6==0) print("<tr>");
                    
                    $foto_creditos = ($foto->getCreditos()) ? " Cr�ditos: ".$foto->getCreditos() : null;
                    $foto_descricao = ($foto->getDescricao()) ? $foto->getDescricao() : null;
                    
                    ?>
                    <td><a href="<?= $link ?>" rel="image-gallery" <?= (($foto_creditos) || ($foto_descricao)) ? "title=\"".$foto_descricao." ".$foto_creditos."\"" : null ?> ><img src="<?= $src ?>" /></a></td>
                    <?php
                    $rowIndex++;
                }
            ?>
        </tbody>
    </table>    
    <?php
        }
        else {
            ?>
    <h3>Galeria n�o encontrada!!!</h3>
            <?php
        }
    ?>
    <p class="voltar"><a href="javascript:history.go(-1);">&lt;&lt;&nbsp;Voltar</a></p>
</section>
<script>
    $(window).load(
        function(){
            $("a[rel='image-gallery']").colorbox(
                { transition:"fade" }
            );
        }
    );
</script>