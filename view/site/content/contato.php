﻿<script>
    $(document).ready(
        function() {
            $("#frm-envia-contato").on("submit",
                function() {
                    var data = $(this).serialize();
                    $("#btt-submit").attr("disabled", "true");
                    $("#msg-frm").css({'display':'none'});
                    $("#msg-frm").html('');
                    $("#msg-frm").removeClass();
                    $("#enviando-frm").toggle();
                    $.ajax(
                        {
                            type: 'POST',
                            url: 'enviar-contato.php',
                            data: data,
                            cache: false,
                            dataType: "json",
                            success: function (json) {
                                if (json.status == "error") {
                                    alert("Contato NÃO enviado!");
                                    $("#msg-frm").addClass('frm-error');
                                    $("#msg-frm").html("Contato NÃO enviado!<br />"+json.message);
                                    $("body").scrollTop("#msg-frm");
                                    $("#msg-frm").effect("pulsate",{'times' : 5},600);
                                    $("body").scrollTop("#msg-frm");
                                }
                                else if  (json.status == "ok") {
                                    alert("Contato enviado com sucesso!");
                                    $("#msg-frm").addClass('frm-success');
                                    $("#msg-frm").html("Contato enviado com sucesso!<br />Em breve responderemos o seu contato.");
                                    $("body").scrollTop("#msg-frm");
                                    $("#msg-frm").effect("pulsate",{'times' : 5},600);
                                    $("#frm-envia-contato").find('input[type="text"]').val("");
                                    $("#frm-envia-contato").find("textarea").val("");
                                    $("#frm-envia-contato").find('input[type="hidden"]').attr("name",json.token);
                                }
                                $("#btt-submit").removeAttr("disabled");
                                $("#enviando-frm").css({'display':'none'});
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $("#btt-submit").removeAttr("disabled");
                                $("#enviando-frm").css({'display':'none'});
                            }
                        }
                    );
                    return false;
                }
            );
            $.mask.definitions['~']='[+-]';
            $('input.phone').mask("(99) 9999-9999?9").ready(function(event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                phone = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if(phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                } else {
                        element.mask("(99) 9999-9999?9");
                }
            });            
        }
    );
</script>
<section class="cols">
    <h3>Contato</h3>
    <div id="msg-frm" style="display: none"></div>
    <form id="frm-envia-contato" method="post">
        <label for="name">Nome</label><span class="required">*</span>
        <br /><input type="text" class="form-field" id="name" name="name" size="40" />
        <br class="block" />
        <label for="email">E-mail</label><span class="required">*</span>
        <br /><input type="text" class="form-field"  id="email" name="email" size="30" />
        <br class="block" />
        <label for="telefone">Telefone</label><span class="required">*</span>
        <br /><input type="text" class="form-field phone" id="telefone" name="telefone" size="20" />
        <br class="block" />
        <label for="message">Mensagem</label><span class="required">*</span>
        <br /><textarea id="message" name="message" class="form-field" cols="45" rows="4"></textarea><br />
        <br class="block" />
        <p id="enviando-frm" style="display: none">Enviando, aguarde...</p>
        <button type="submit" class="button" id="btt-submit">Enviar</button>
    </form>
    <p class="voltar"><a href="javascript:history.go(-1);">&lt;&lt;&nbsp;Voltar</a></p>
</section>