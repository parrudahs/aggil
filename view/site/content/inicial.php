<?php
    $empresa = new Jcms\Core\Controllers\InstitucionalController();
    $e = $empresa->index('EMPRESA');
    $s = $empresa->index('SERVICOS');
?>
<!-- cols -->
<section class="cols">
    <div class="col">
        <h3><a href="#">Empresa</a></h3>
        <?= $e['chamada']; ?>
        <a href="empresa" class="col-btn">Saiba mais</a>
    </div>
    <div class="col">
        <h3><a href="#">Serviços</a></h3>
        <?= $s['chamada']; ?>
        <a href="servicos" class="col-btn">Saiba mais</a>
    </div>
    <div class="col">
        <h3><a href="#">Notícias</a></h3>
        <?php

            $noticia_destaque = new \Jcms\Core\Controllers\NoticiaController();
            $n = $noticia_destaque->index();
        ?>
        <strong><?= $n['titulo']; ?></strong>
        <br class="block" />
        <?php
            if (($n['imagem_destaque']) && (@is_file("public/uploaded_files/noticias/".$n['imagem_destaque']))) {
                ?>
        <img src="public/uploaded_files/noticias/140x90-<?= $n['imagem_destaque'] ?>" class="imagem-destaque" />
                <?php
            }
        ?>
        <?= $n['resumo'] ?>
        <br class="block" />
        <a href="<?= BASE_URL ?>ver-noticia/<?= $n['noticia_id']."/". Jcms\Core\Ext\Content::gerarURL($n['titulo']) ?>" class="col-btn">Ler mais</a>
    </div>
    <div class="cl">&nbsp;</div>
</section>
<!-- end of cols  -->

<!-- box -->
<section class="box">
    <?php    
        $galeria = new Jcms\Core\Controllers\GaleriaController();
        $g = $galeria->index();

        if ($galeria->getRowCount() > 0) {
                    
            $foto = new Jcms\Core\Controllers\FotoController();
            $fotos = $foto->index($g['galeria_id']);
    ?>
    <span class="shadow-t"></span>
    <h3><?= $g['titulo'] ?></h3>
    <a href="galerias" class="alignright"> + Galerias de Fotos</a>
    <div class="cl">&nbsp;</div>
    <!-- entries -->
    <div class="entries">
            <?php
            if($foto->getRowCount() > 0 ) {
                foreach ($fotos as $f) {
                    $src =  "public/uploaded_files/galerias/200x200-".$f['arquivo'];
                    $link = "public/uploaded_files/galerias/1024x768-".$f['arquivo'];
                    $foto_creditos = ($f['creditos']) ? " Créditos: ".$f['creditos'] : null;
                    $foto_descricao = ($f['descricao']) ? $f['descricao'] : null;
            ?>
                <div class="entry">
                    <a href="<?= $link ?>" rel="image-gallery" <?= (($foto_creditos) || ($foto_descricao)) ? "title=\"".$foto_descricao." ".$foto_creditos."\"" : null ?> ><img src="<?= $src ?>" /></a>
                    <span class="shadow"></span>
                </div>
        <?php
                }
            }
        ?>
        <div class="cl">&nbsp;</div>
        <span class="shadow-b"></span>
    </div>
    <script>
        $(window).load(
            function(){
                $("a[rel='image-gallery']").colorbox(
                    { transition:"fade" }
                );
            }
        );
    </script>    
    <?php
        }
    ?>
</section>
<!-- end of box -->