<?php
    $noticia = new \Jcms\Core\Controllers\NoticiaController();
    $n = $noticia->getNoticia($urls[1]);
?>
<section class="cols">
    <h2>Noticias</h2>
    <?php
        if ($noticia->getRowCount() > 0) {
    ?>
    <h3> <?= $n['titulo'] ?></h3>
    <div>
        <?php
            if ($n['imagem_destaque']) {
                ?>
                <p style="text-align: center">
                    <a href="#">
                        <img src="<?= BASE_URL ?>public/uploaded_files/noticias/400x300-<?= $n['imagem_destaque'] ?>" />
                    </a>
                </p>
                <?php
            }
        ?>
        <?= $n['resumo'] ?>
    </div>
    <?= $n['conteudo'] ?>
    <h3>Outras notícias</h3>
    <ul class="lista-noticias">
        <?php
            $outra_noticia = $noticia->getNoticiasOutras($urls[1]);
            foreach ($outra_noticia as $no) {
        ?>
            <li><a href="<?= BASE_URL ?>ver-noticia/<?= $no['noticia_id']."/".Jcms\Core\Ext\Content::gerarURL($no['titulo']) ?>"><?= $no['titulo'] ?></a></li>
        <?php
            }  
        ?>
    </ul>    
    <?php
        }
        else {
            ?>
    <h3>Not&iacute;cias não encontrada!!!</h3>
            <?php
        }
    ?>
    <p class="voltar"><a href="javascript:history.go(-1);">&lt;&lt;&nbsp;Voltar</a></p>
</section>
<script>
    $(window).load(
        function(){
            $("a.foto-destaque").colorbox();
        }
    );
</script>