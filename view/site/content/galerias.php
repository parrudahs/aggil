<section class="cols">
    <h2>Galeria de fotos</h2>
    <dl id="lista-galerias">
        <?php
        $galeria = new Jcms\Core\Controllers\GaleriaController();
        $galerias = $galeria->todasAsGalerias();
        $rowCount= $galeria->getRowCount();
        $rowIndex = 0;

        $foto = new Jcms\Core\Controllers\FotoController();

        foreach ($galerias as $gf) {
            $foto_capa = $foto->capaGaleria($gf['galeria_id']);
            
            if (($rowIndex%4)==0) echo "<tr>";
            $rowIndex++;
            if($foto->getRowCount() > 0)
                $capa =  "public/uploaded_files/galerias/200x200-".$foto_capa['arquivo'];
            else
                $capa = null;
            ?>
            <dt>
                <a href="galeria/<?= $gf['galeria_id']."/".Jcms\Core\Ext\Content::gerarURL($gf['titulo']) ?>"><img src="<?= $capa ?>" /></a>
            </dt>
            <dd>
                <strong><?= $gf['titulo'] ?></strong>
                <?= ($gf['data_fotos']) ? "<br/><strong>".date("d.m.Y",$gf['data_fotos'])."</strong>" : null ?>
                <br/>
                <?= $gf['descricao'] ?>
                <p>
                    <a href="galeria/<?= $gf['galeria_id']."/".Jcms\Core\Ext\Content::gerarURL($gf['titulo']) ?>">Ver galeria</a>
                </p>
            </dd>
        <?php
        }
        ?>
    </dl>
    <p class="voltar"><a href="javascript:history.go(-1);">&lt;&lt;&nbsp;Voltar</a></p>
</section>