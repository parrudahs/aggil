﻿<?php
    require_once __DIR__ . '/vendor/autoload.php';
    require __DIR__ . '/Jcms/setup/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="iso-8859-1" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Aggil - Transportes e Logística</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= BASE_URL ?>css/images/favicon.ico" />
        <link rel="stylesheet" href="<?= BASE_URL ?>css/style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE_URL ?>css/colorbox.css" type="text/css" media="all" />
        <script src="<?= BASE_URL ?>js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="<?= BASE_URL ?>js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
        <script src="<?= BASE_URL ?>js/jquery.maskedinput.min.js"></script>
        <!--[if lt IE 9]>
                <script src="<?= BASE_URL ?>js/modernizr.custom.js"></script>
        <![endif]-->
        <script src="<?= BASE_URL ?>js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="<?= BASE_URL ?>js/jquery.colorbox.js"></script>
        <script src="<?= BASE_URL ?>js/functions.js" type="text/javascript"></script>
    </head>
    <body>
        <!-- wrapper -->
        <div id="wrapper">
            <!-- shell -->
            <div class="shell">
                <!-- container -->
                <div class="container">
                    <!-- header -->
                    <header class="header">
                        <h1 id="logo"><a href="<?= BASE_URL ?>index.php">Logo</a></h1>
                        <nav id="navigation">
                            <?php
                            
                                $requested_url=isset($_GET['content']) ? $_GET['content'] : "";
                                include("parts/menu-principal.php");
                                //echo $requested_url;
                            ?>
                        </nav>
                        <div class="cl">&nbsp;</div>
                    </header>
                    <!-- end of header -->
                    <div class="main">
                        <!-- slider -->
                        <div class="flexslider">
                            <!-- <img src="<?= DIR_PAGE ?>/css/images/slide-img-interno.jpg" width="892" height="331" alt="" /> -->
                            <?php
                                $d = new Jcms\Core\Model\Dao\DestaquesDAO();
                                var_dump($d->listItemsDAO(0,0));
                            ?>
                                    <img src="/<?= DIR_PAGE ?>imagens/destaques/<?= $destaque->getImagem() ?>" />
                                    <?php
                                    ?>
                                <img src="<?= DIR_PAGE ?>css/images/slide-img-interno.jpg" width="892" height="331" alt="" />
                        </div>
                        <!-- end of slider -->
                        <?php
                            include("switchContent.php");
                            include("content/".$requested_file);
                        ?>
                        <!-- services -->
                        <section class="services">
                            <div class="widget">
                                <h3>Aggil Transportes LTDA - ME</h3>
                                <ul>
									<li>Rua Bela Vista do Paraiso, 1782</li>
									<li>Jardim Nova Portugal</li>
									<li>CEP 07171-000 - Garulhos – SP</li>
									<li>Telefone: (11) 4016-3521 (DISCAR DDD)</li>
									<li>Whatsapp: (11) 96831-2093 (SOMENTE WHATSAPP)</li>
									<li>E-mail: <a href="mailto:edson@aggiltransportes.com.br?subject=Contato-Site">edson@aggiltransportes.com.br</a></li>
                                </ul>
                            </div>
                            <div class="widget socials-widget">
                                <?php
                                    include("parts/social.php");
                                ?>
                            </div>
                            <div class="cl">&nbsp;</div>
                        </section>
                        <!-- end of services -->
                    </div>
                    <!-- end of main -->
                </div>
                <!-- end of container -->	
                <div class="footer">
                    <nav class="footer-nav">
                        <?php
                            include("parts/menu-rodape.php");
                        ?>
                    </nav>
                    <p class="copy">Aggil Transportes e Logística - Copyright &copy; 2014 - Todos os direitos reservados.</p>
                </div>
            </div>
            <!-- end of shell -->
        </div>
        <!-- end of wrappert -->
    </body>
</html>