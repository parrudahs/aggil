<?php
//error_reporting(0);
//error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED);
error_reporting(E_ALL);
session_start();
date_default_timezone_set("America/Sao_Paulo");

/**
 * Configurações básicas
 */
define('BASE_URL', 'http://localhost/aggil/');
define('ERROR_404', '404.php');

/**
 * Configuração do banco de dados
 */
define("DB_HOST", "localhost");
define("DB_NAME", "jvmkt_aggil");
define("DB_USER", "root");
define("DB_PASS", "55");
define("BD_TYPE", "mysql");

/**
 * O usuário do sistema, para acesso por login.
 */
define("USUARIO", "aggil");

/**
 * Configuração de envio de email
 */
define("EMAIL_SENDER", "");
define("EMAIL_SMTP_HOST", "");
define("EMAIL_SMTP_PORT", "");
define("EMAIL_SMTP_LOGIN", "");
define("EMAIL_SMTP_PASS", "");
?>
