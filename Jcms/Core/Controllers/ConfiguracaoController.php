<?php
/**
 * Created by PhpStorm.
 * User: paulo
 * Date: 17/03/17
 * Time: 20:44
 */

namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\ConfiguracoesDAO;
use Jcms\Core\Models\Configuracao;
use Jcms\Core\Auth\Autenticacao;

class ConfiguracaoController extends ConfiguracoesDAO
{
    /**
     * @return mixed
     */
    public function indexPainel()
    {
        $configs = array();
        $rowIndex=0;
        $conf = $this->listItems(0,0,true);
        $numRows= $this->getRowCount();
        while ($rowIndex < $numRows) {
            $configs[$conf[$rowIndex]['name']] = $conf[$rowIndex]['value'];
            $rowIndex++;
        }
        return $configs;
    }

    /**
     * Metodo que faz a atualização da senha e emails para acessar o sistema. 
     */
    public function updateConfiguracao(array $dados) {
        if($dados['senha'] != $dados['re-senha']) {
            $_SESSION['output_message'] = 'As senhas não confere!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        // valida e-mail
        $emails = explode(',', $dados['emails']);
        foreach ($emails as $e) {
            if(!$this->validaemail($e)){
                $_SESSION['output_message'] = 'Email Inválido!';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }                
        }

        $config = new Configuracao();
        $config->setName('PASSWORD_CMS');
        $config->setValue(md5($dados['senha']));

        $config1 = new Configuracao();
        $config1->setName('EMAILS_CONTATO');
        $config1->setValue($dados['emails']);

        //atualiza apenas os emails, caso a senha esteja vazio .
        if($dados['senha'] == null) {
            if($this->updateItem($config1, 2)){
                $_SESSION['output_message'] = 'Configurações atualizadas com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }
        }
        // atualia senha e emails.
        if($this->updateItem($config, 1) && $this->updateItem($config1, 2)) {
            $_SESSION['output_message'] = 'Configurações atualizadas com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }   
        return 0;
    }

    //função de validação de e-mail
    private function validaemail($email){
        if(!preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $email)) {
            return 0;
        }
        return 1;
    }
}