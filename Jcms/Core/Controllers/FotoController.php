<?php
/**
 * Created by PhpStorm.
 * User: paulo
 * Date: 13/03/17
 * Time: 15:44
 */

namespace Jcms\Core\Controllers;


use Jcms\Core\Models\Dao\FotosDAO;
use Jcms\Core\Models\Foto;
use Intervention\Image\ImageManagerStatic as Image;

class FotoController extends FotosDAO
{

    private $foto;
    private $diretorioUpload;

    public function __construct() 
    {
        $this->foto = new Foto();
        $this->diretorioUpload = "public/uploaded_files/galerias/";
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function index($id)
    {
        $this->clearFilter();
        $this->addFilter(array("galeria_id","=",$id));
        $this->addSorter(array("RAND()",""));
        return $this->listItems(0,4, true);
    }

    /**
     * 
     */
    public function capaGaleria($id)
    {
        $this->clearFilter();
        $this->addFilter(array("galeria_id","=",$id));
        $this->addFilter(array("capa","=",1));
        $this->addSorter(array("RAND()",""));
        $retorno = $this->listItems(0,1, true);
        return $retorno[0];
    }

    /**
     * Lista Todas as fotos de uma Galeria
     */
    public function getFotosGaleria($id) 
    {
        $this->addFilter(array('galeria_id','=',$id));
        return $this->listItems(0,0, true);
    } 


    /**
     * 
     */
    public function Formulario($dados) 
    {
        $this->foto->setGaleriaID(isset($dados['galeria_id']) ? $dados['galeria_id']: 0);
        $this->foto->setCreditos(isset($dados['cred']) ? $dados['cred']: null);
        $this->foto->setArquivo(isset($dados['arquivo']) ? $dados['arquivo']: null);
        $this->foto->setDescricao(isset($dados['descricao']) ? $dados['descricao']: null);
        $this->foto->setCapa(isset($dados['capa']) ? $dados['capa']: '0');
        $this->foto->setPublicada(isset($dados['publicada']) ? $dados['publicada'] : '0');
    }

    /**
     * Metodo que adiciona as fotos
     */
    public function addFotos($id)
    {
        if(!empty($_FILES['file'])) {
            $temp = explode(".",  $_FILES['file']['name']);
            $data['arquivo'] = time().uniqid(rand()).'.'.end($temp);
            $data['galeria_id'] = $id;
            $data['capa'] = '0';

            $this->Formulario($data);

            if( $_FILES['file']['type'] == 'image/jpeg' || 
                 $_FILES['file']['type'] == 'image/png' || 
                 $_FILES['file']['type'] == 'image/gif'){
                if($this->insertItem($this->foto)) {
                    $upload_arq = $this->diretorioUpload."200x200-".$data['arquivo'];
                    if(Image::make( $_FILES['file']['tmp_name'])->resize(200,200)->save($upload_arq)){
                        $upload_arq = $this->diretorioUpload."1024x768-".$data['arquivo'];
                        Image::make( $_FILES['file']['tmp_name'])->resize(1024,768)->save($upload_arq);
                        
                        $_SESSION['output_message'] = 'As fotos foram adicionadas com sucesso!';
                        $_SESSION['output_message_tipo'] = 'success';
                        return 1;
                    }
                }
            } else {
                $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        } else {
            $_SESSION['output_message'] = 'Erro';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

    /**
     * Metodo que mostra uma Foto
     */
    public function show($id)
    {
        $this->addFilter(array('foto_id', '=', $id));
        $data = $this->listItems(0,1);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * Atualiza Foto
     */
    public function update($data, $id)
    {
        $data['capa'] = ($data['capa'] == 'on') ? '1' : '0';
        $data['publicada'] = ($data['publicada'] == 'on') ? '1' : '0';
        
        $this->Formulario($data);   
        if($this->updateItem($this->foto, $id)) {
            $_SESSION['output_message'] = 'Foto atualizada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar a Foto!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;       
    }

    /**
     * Deletar Foto
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload."200x220-".$imagem['arquivo']);
            unlink($this->diretorioUpload."1024x768-".$imagem['arquivo']);

            $_SESSION['output_message'] = 'A foto foi deletada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        } else {
            $_SESSION['output_message'] = 'Erro ao deletar a noticia!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }


}