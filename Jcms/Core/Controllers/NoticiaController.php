<?php
namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\NoticiasDAO;
use Jcms\Core\Models\Dao\CategoriasDAO;
use Jcms\Core\Models\Noticia;
use Jcms\Core\Ext\Content;
use Jcms\Core\Ext\DataHr;
use Intervention\Image\ImageManagerStatic as Image;

class NoticiaController extends NoticiasDAO
{

    private $categoria;
    private $noticia;
    private $diretorioUpload; 

    public function __construct()
    {
        $this->noticia = new Noticia();
        $this->categoria = new CategoriasDAO();
        $this->diretorioUpload = 'public/uploaded_files/noticias/';
    }

    /**
     * @return mixed
     */
    public function index() 
    {
        $this->addFilter(array("publicada","=",1));
        $this->addFilter(array("destaque","=",1));
        $this->addSorter(array("RAND()",""));
        $retorno = $this->listItems(0,1);
        return $retorno[0];
    }

    /**
     * Lista todas as categorias
     */
    public function getCategorias()
    {
        return $this->categoria->listItems(0,0);
    }

    /**
     * 
     */
    public function show($id)
    {
        $this->addFilter(array('noticia_id', '=', $id));
        $data = $this->listItems(0,1);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * Retorna apenas uma noticia no site.
     */
    public function getNoticia($id) 
    {
        $this->addFilter(array('noticia_id', '=', $id));
        $data = $this->listItems(0,1, true);
        return $data[0];
    }

    /**
     * Retorna as noticias da pagina de ver-noticia
     */
    public function getNoticiasOutras($idatual)
    {
        $this->clearFilter();
        $this->clearSorter();        
        $this->addSorter(array("data_noticia","DESC"));
        $this->addFilter(array("noticia_id","!=",$idatual));
        $this->addFilter(array("publicada","=",1));
        return $this->listItems(0,8);         
    }

    /**
     * todas as noticias
     * @param null $paginacao
     * @return mixed
     */
    public function all() 
    {
        //$pagina = $pagina != null ? (int) $pagina : 1;
        //$rowPerPage = 4;
        $this->addSorter(array("data_noticia","DESC"));
        //return $this->listItemsPDO((($pagina-1)*$rowPerPage),$rowPerPage, true);
        return $this->listItems(0,0,true);
    }

    public function test($pagina, $rowPerPage)
    {
        //$pagina = $pagina != null ? (int) $pagina : 1;
        $this->addSorter(array("data_noticia","DESC"));
        return $this->listItems((($pagina-1)*$rowPerPage),$rowPerPage, true);
        //return $this->listItems(0,0,true);
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_noticia']);

        $this->noticia->setURL(Content::gerarURL($data['titulo']));
        $this->noticia->setCategoriaID(isset($data['categoria_id']) ? $data['categoria_id'] : null);
        $this->noticia->setTitulo($data['titulo']);
        $this->noticia->setResumo(isset($data['resumo']) ? $data['resumo'] : null);
        $this->noticia->setConteudo(isset($data['conteudo']) ? $data['conteudo'] : null);
        $this->noticia->setDataNoticia(
            DataHr::toUnixTime(
                substr(isset($data['data_noticia']) ? $data['data_noticia'] : null,0,10), 
                substr(isset($data['data_noticia']) ? $data['data_noticia'] : null,11,5)
            )
        );
        $this->noticia->setImagemDestaque(isset($data['imagem_destaque']) ? $data['imagem_destaque'] : null);
        $this->noticia->setPublicada(isset($data['publicada']) ? $data['publicada'] : '0');
        $this->noticia->setDestaque(isset($data['destaque']) ? $data['destaque'] : '0');

        $_SESSION['formulario_noticia']['titulo'] = $this->noticia->getTitulo();
        $_SESSION['formulario_noticia']['categoria_id'] = $this->noticia->getCategoriaID();
        $_SESSION['formulario_noticia']['resumo'] = $this->noticia->getResumo();
        $_SESSION['formulario_noticia']['conteudo'] = $this->noticia->getConteudo();
        $_SESSION['formulario_noticia']['data_noticia'] = $this->noticia->getDataNoticia();
        $_SESSION['formulario_noticia']['publicada'] = $this->noticia->getPublicada();
        $_SESSION['formulario_noticia']['destaque'] = $this->noticia->getDestaque();
        $_SESSION['formulario_noticia']['imagem'] = $this->noticia->getImagemDestaque();
    }

    /**
     * 
     */
    public function create($data) 
    {
        $temp = explode(".", $_FILES['imagem']['name']);
        $data['imagem_destaque'] = time().uniqid(rand()).'.'.end($temp);

        $this->Formulario($data);

        if($_FILES['imagem']['type'] == 'image/jpeg' || 
           $_FILES['imagem']['type'] == 'image/png' || 
           $_FILES['imagem']['type'] == 'image/gif')
        {
            if (!empty($this->noticia->getTitulo()) && !empty($this->noticia->getConteudo())) {
                if($this->insertItem($this->noticia)) {
                    $upload_arq = $this->diretorioUpload.$data['imagem_destaque'];                  
                    if(Image::make($_FILES['imagem']['tmp_name'])->resize(1200,900)->save($upload_arq)) {
                        $upload_arq = $this->diretorioUpload."400x300-".$data['imagem_destaque'];
                        Image::make($_FILES['imagem']['tmp_name'])->resize(400,300)->save($upload_arq);
                        $upload_arq = $this->diretorioUpload."140x90-".$data['imagem_destaque'];
                        Image::make($_FILES['imagem']['tmp_name'])->resize(140,90)->save($upload_arq);
                        $_SESSION['output_message'] = 'Noticia cadastrada com sucesso!';
                        $_SESSION['output_message_tipo'] = 'success';
                        unset($_SESSION['formulario_noticia']);
                        return 1;
                    } else {
                        $_SESSION['output_message'] = 'erro no upload da imagem!';
                        $_SESSION['output_message_tipo'] = 'danger';
                        return 0;
                    }
                }else {
                    $_SESSION['output_message'] = 'Não foi possível cadastrar a noticia!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            } else {
                $_SESSION['output_message'] = 'Noticia não cadastrada!<br/>Campos marcados com * são obrigatórios.';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        }else {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        return 0;
    }

    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] == 'image/jpeg' || 
           $_FILES['imagem']['type'] == 'image/png' || 
           $_FILES['imagem']['type'] == 'image/gif')
        {
            $temp = explode(".", $_FILES['imagem']['name']);
            $data['imagem_destaque'] = time().uniqid(rand()).'.'.end($temp);
            //remove a imagem do servidor
            $image = $this->show($id);

            unlink($this->diretorioUpload.$image['imagem_destaque']);
            unlink($this->diretorioUpload."400x300-".$image['imagem_destaque']);
            unlink($this->diretorioUpload."140x90-".$image['imagem_destaque']);

            $this->Formulario($data);
            if($this->updateItem($this->noticia, $id)) {
                $upload_arq = $this->diretorioUpload.$data['imagem_destaque']; 
                 if(Image::make($_FILES['imagem']['tmp_name'])->resize(1200,900)->save($upload_arq)) {
                    $upload_arq = $this->diretorioUpload."400x300-".$data['imagem_destaque'];
                    Image::make($_FILES['imagem']['tmp_name'])->resize(400,300)->save($upload_arq);
                    $upload_arq = $this->diretorioUpload."140x90-".$data['imagem_destaque'];
                    Image::make($_FILES['imagem']['tmp_name'])->resize(140,90)->save($upload_arq);
                    $_SESSION['output_message'] = 'Noticia atualizada com sucesso!';
                    $_SESSION['output_message_tipo'] = 'success';
                    return 1;
                } else {
                    $_SESSION['output_message'] = 'erro no upload da imagem!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            } else {
                $_SESSION['output_message'] = 'Não foi possível atualizar a noticia!';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        } else {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

    /**
     * Método para atualizar um destaque
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (!empty($this->noticia->getTitulo())) { 
            if(empty($_FILES['imagem']['name'])) {
                $image = $this->show($id);
                $data['imagem_destaque'] = $image['imagem_destaque'];  
                $this->Formulario($data);       
                if($this->updateItem($this->noticia, $id)) {
                    $_SESSION['output_message'] = 'Noticia atualizada com sucesso!';
                    $_SESSION['output_message_tipo'] = 'success';
                    return 1;
                } else {
                    $_SESSION['output_message'] = 'Não foi possível atualizar a noticia!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            }else {
                $this->updateImagem($data, $id);
            }    
        } else {
            $_SESSION['output_message'] = 'Noticia não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

    /**
     * 
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload.$imagem['imagem_destaque']);
            unlink($this->diretorioUpload."400x300-".$imagem['imagem_destaque']);
            unlink($this->diretorioUpload."140x90-".$imagem['imagem_destaque']);

            $_SESSION['output_message'] = 'Noticia deletada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        } else {
            $_SESSION['output_message'] = 'Erro ao deletar a noticia!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

}