<?php
namespace Jcms\Core\Controllers;


use Jcms\Core\Models\Dao\GaleriasDAO;
use Jcms\Core\Models\Dao\CategoriasDAO;
use Jcms\Core\Models\Galeria;
use Jcms\Core\Ext\DataHr;

class GaleriaController extends GaleriasDAO
{


    private $categoria;
    private $galeria;
    private $diretorioUpload;

    /**
     * 
     */
    public function __construct()
    {
        $this->categoria = new CategoriasDAO();
        $this->galeria = new Galeria();
        $this->diretorioUpload = "public/uploaded_files/galerias/";
    }


    /**
     * 
     */
    public function getCategorias()
    {
        return $this->categoria->listItems(0,0);
    }

    /**
     * @return array|mixed
     */
    public function index()
    {
        $this->clearFilter();
        $this->addFilter(array("publicada","=",1));
        $this->addFilter(array("destaque","=",1));
        $this->addSorter(array("RAND()",""));
        $retorno = $this->listItems(0,1, true);
        return $retorno[0];
    }

    /**
     * @return array|mixed
     */
    public function todasAsGalerias()
    {
        $this->clearFilter();
        $this->addSorter(array("data_fotos","DESC"));
        $this->addSorter(array("titulo","ASC"));
        return $this->listItems(0,0,true);
    }

    /**
     * 
     */
    public function show($id)
    {
        $this->addFilter(array('galeria_id', '=', $id));
        $data = $this->listItems(0,0);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_gal']);

        $this->galeria->setCategoriaID(isset($data['categoria_id']) ? $data['categoria_id'] : null);
        $this->galeria->setTitulo($data['titulo']);
        $this->galeria->setDescricao(isset($data['descricao']) ? $data['descricao'] : null);
        $this->galeria->setTexto(isset($data['texto']) ? $data['texto'] : null);
        $this->galeria->setDatafotos(DataHr::toUnixTime(isset($data['data_fotos']) ? $data['data_fotos'] : null, 0));
        $this->galeria->setPublicada(isset($data['publicada']) ? $data['publicada'] : '0');
        $this->galeria->setNoticia(isset($data['noticia']) ? $data['noticia'] : '0');
        $this->galeria->setDestaque(isset($data['destaque']) ? $data['destaque'] : '0');

        $_SESSION['formulario_gal']['titulo'] = $this->galeria->getTitulo();
        $_SESSION['formulario_gal']['categoria_id'] = $this->galeria->getCategoriaID();
        $_SESSION['formulario_gal']['descricao'] = $this->galeria->getDescricao();
        $_SESSION['formulario_gal']['texto'] = $this->galeria->getTexto();
        $_SESSION['formulario_gal']['data_fotos'] = $this->galeria->getDatafotos();
        $_SESSION['formulario_gal']['publicada'] = $this->galeria->getPublicada();
        $_SESSION['formulario_gal']['destaque'] = $this->galeria->getDestaque();
        $_SESSION['formulario_gal']['noticia'] = $this->galeria->getNoticia();
    }

    /**
     * 
     */
    public function create($data) 
    {
        $this->Formulario($data);
        if (!empty($this->galeria->getTitulo())) {
            if($this->insertItem($this->galeria)) {       
                $_SESSION['output_message'] = 'Galeria cadastrada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                unset($_SESSION['formulario_gal']);
                return 1;                
            }else {
                $_SESSION['output_message'] = 'Não foi possível cadastrar a galeria!';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        } else {
            $_SESSION['output_message'] = 'Galeria não cadastrada!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        return 0;
    }

    /**
     * 
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (!empty($this->galeria->getTitulo())) {            
            if($this->updateItem($this->galeria, $id)) {
                $_SESSION['output_message'] = 'Galeria atualizada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            } else {
                $_SESSION['output_message'] = 'Não foi possível atualizar a galeria!';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }   
        } else {
            $_SESSION['output_message'] = 'Galeria não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

    /**
     * 
     */
    public function delete($id)
    {
        $this->addFilter(array('galeria_id', '=', $id));
        $imagens = $this->listFotos(0,0, true);

        if($this->getRowCount() > 0) {
            foreach ($imagens as $i) {
                unlink($this->diretorioUpload."200x200-".$i['arquivo']);
                unlink($this->diretorioUpload."1024x768-".$i['arquivo']);
            }
            $this->deleteFotos($id);
            if($this->deleteItem($id)) {
                $_SESSION['output_message'] = 'Galeria deletada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }
        } else {
            if($this->deleteItem($id)) {                
                $_SESSION['output_message'] = 'Galeria deletada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }
        }
        $_SESSION['output_message'] = 'Erro ao deletar galeria!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }


}