<?php


namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\InstitucionaisDAO;
use Jcms\Core\Models\Institucional;
use Intervention\Image\ImageManagerStatic as Image;

class InstitucionalController extends InstitucionaisDAO
{

    private $institucional;
    private $diretorioUpload; 

    public function __construct()
    {
        $this->institucional = new Institucional();
        $this->diretorioUpload = 'public/uploaded_files/institucional/';
    }

    /**
     * @return array|mixed
     */
    public function index($tipo)
    {
        //limpa filter
        $this->clearFilter();
        //adiciona filter
        $this->addFilter(array('nome', '=', $tipo));
        //lista itens
        $retorno = $this->listItems(0,0);
        return $retorno[0];
    }

    /**
     * @return mixed
     */
    public function all()
    {
        $this->addSorter(array("nome","ASC"));
        return $this->listItems(0,0,true);
    }

    /**
     * 
     */
    public function show($id)
    {
        $this->addFilter(array('conteudo_id', '=', $id));
        $data = $this->listItems(0,1);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_inst']);
        $this->institucional->setTitulo(isset($data['titulo']) ? $data['titulo'] : null);
        $this->institucional->setSubTitulo(isset($data['sub-titulo']) ? $data['sub-titulo'] : null);
        $this->institucional->setChamada(isset($data['chamada']) ? $data['chamada'] : null);
        $this->institucional->setConteudo(isset($data['conteudo']) ? $data['conteudo'] : null);
        $this->institucional->setImagemDestaque(isset($data['imagem_destaque']) ? $data['imagem_destaque'] : null);

        $_SESSION['formulario_inst']['titulo'] = $this->institucional->getTitulo();
        $_SESSION['formulario_inst']['sub-titulo'] = $this->institucional->getSubTitulo();
        $_SESSION['formulario_inst']['chamada'] = $this->institucional->getChamada();
        $_SESSION['formulario_inst']['conteudo'] = $this->institucional->getConteudo();
    }


    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] == 'image/jpeg' || 
           $_FILES['imagem']['type'] == 'image/png' || 
           $_FILES['imagem']['type'] == 'image/gif')
        {
            $temp = explode(".", $_FILES['imagem']['name']);
            $data['imagem_destaque'] = time().uniqid(rand()).'.'.end($temp);

            //remove a imagem do servidor
            $image = $this->show($id);
            unlink($this->diretorioUpload.$image['imagem_destaque']);

            $this->Formulario($data);

            if($this->updateItem($this->institucional, $id)) {

                $imagem = Image::make($_FILES['imagem']['tmp_name']);

                if($imagem->width() != 265 || $imagem->height() != 77)
                    $imagem->resize(265,77);

                if($imagem->save($this->diretorioUpload.$data['imagem_destaque'])) {

                    $_SESSION['output_message'] = 'Conteúdo atualizado com sucesso!';
                    $_SESSION['output_message_tipo'] = 'success';
                    unset($_SESSION['formulario_inst']);
                    return 1;

                } else {
                    $_SESSION['output_message'] = 'erro no upload da imagem!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            } else {
                $_SESSION['output_message'] = 'Não foi possível atualizar o conteudo!';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        } else {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        return 0;
    }

    /**
     * Método para atualizar
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (!empty($this->institucional->getTitulo()) && !empty($this->institucional->getConteudo())) {
            if(empty($_FILES['imagem']['name'])) { 
                $imagem = $this->show($id);
                $data['imagem_destaque'] = $imagem['imagem_destaque'];
                $this->Formulario($data);
                if($this->updateItem($this->institucional, $id)) {
                    $_SESSION['output_message'] = 'Conteúdo atualizado com sucesso!';
                    $_SESSION['output_message_tipo'] = 'success';
                    unset($_SESSION['formulario_inst']);
                    return 1;
                } else {
                    $_SESSION['output_message'] = 'Não foi possível atualizar o conteudo!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            } else {
                $this->updateImagem($data, $id);
            }    
        } else {
            $_SESSION['output_message'] = 'Conteudo não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

}