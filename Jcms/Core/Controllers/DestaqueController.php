<?php

namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\DestaquesDAO;
use Jcms\Core\Models\Destaque;
use Intervention\Image\ImageManagerStatic as Image;

class DestaqueController extends DestaquesDAO
{

    private $destaque;
    private $diretorioUpload; 

    public function __construct() {
        $this->destaque = new Destaque();
        $this->diretorioUpload = 'public/uploaded_files/destaque/';
    }

    /**
     * lista todos os dados da tabela destaque na pagina inicial do site
     * @return array|mixed
     */
    public function index() 
    {
        $this->addSorter(array("destaque_id","DESC"));
        return $this->listItems(0,0);
    }

    /**
     * @return array|mixed
     */
    public function listAdmin() 
    {
        $this->addSorter(array("publicado","DESC"));
        $this->addSorter(array("ordem","ASC"));
        return $this->listItems(0,0, true);
    }


    /**
     * @param $id
     */
    public function show($id)
    {
        $this->addFilter(array('destaque_id', '=', $id));
        $data = $this->listItems(0,1,true);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * comentar metodo
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_destaque']);

        $this->destaque->setTitulo(isset($data['titulo']) ? $data['titulo'] : null);
        $this->destaque->setOrdem(isset($data['ordem']) ? $data['ordem'] : null);
        $this->destaque->setDescricao(isset($data['descricao']) ? $data['descricao'] : null);
        $this->destaque->setImagem(isset($data['imagem']) ? $data['imagem'] : null);
        $this->destaque->setComportamento(isset($data['comportamento']) ? $data['comportamento'] : null);
        $this->destaque->setURL(isset($data['url']) ? $data['url'] : null);
        $this->destaque->setPublicado(isset($data['publicado']) ? $data['publicado'] : '0');

        $_SESSION['formulario_destaque']['titulo'] = $this->destaque->getTitulo();
        $_SESSION['formulario_destaque']['ordem'] = $this->destaque->getOrdem();
        $_SESSION['formulario_destaque']['descricao'] = $this->destaque->getDescricao();
        $_SESSION['formulario_destaque']['comportamento'] = $this->destaque->getComportamento();
        $_SESSION['formulario_destaque']['publicado'] = $this->destaque->getPublicado();
        $_SESSION['formulario_destaque']['url'] = $this->destaque->getURL();
    }

    /**
     * Cadastro de Destaque
     * @param $data = $_POST
     */
    public function create($data)
    {
        $temp = explode(".", $_FILES['imagem']['name']);
        $data['imagem'] = time().uniqid(rand()).'.'.end($temp);

        $this->Formulario($data);

        if($_FILES['imagem']['type'] == 'image/jpeg' || 
           $_FILES['imagem']['type'] == 'image/png' || 
           $_FILES['imagem']['type'] == 'image/gif')
        {
            if (!empty($this->destaque->getTitulo())) {            
                if($this->insertItem($this->destaque)) {
                    $upload_arq = $this->diretorioUpload.$data['imagem'];
                    $imagem = Image::make($_FILES['imagem']['tmp_name']);
                    if($imagem->save($upload_arq)) {
                        $_SESSION['output_message'] = 'Destaque cadastrada com sucesso!';
                        $_SESSION['output_message_tipo'] = 'success';
                        unset($_SESSION['formulario_destaque']);
                        return 1;
                    } else {
                        $_SESSION['output_message'] = 'erro no upload da imagem!';
                        $_SESSION['output_message_tipo'] = 'danger';
                        return 0;
                    }
                }else {
                    $_SESSION['output_message'] = 'Não foi possível cadastrar o destaque!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            } else {
                $_SESSION['output_message'] = 'Destaque não cadastrado!<br/>Campos marcados com * são obrigatórios.';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        }else {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        return 0;
    }

    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] == 'image/jpeg' || 
           $_FILES['imagem']['type'] == 'image/png' || 
           $_FILES['imagem']['type'] == 'image/gif')
        {
            $temp = explode(".", $_FILES['imagem']['name']);
            $data['imagem'] = time().uniqid(rand()).'.'.end($temp);
            //remove a imagem do servidor
            $image = $this->show($id);

            unlink($this->diretorioUpload.$image['imagem']);
            $this->Formulario($data);
            if($this->updateItem($this->destaque, $id)) {
                if(move_uploaded_file($_FILES['imagem']['tmp_name'], $this->diretorioUpload.$data['imagem'])) {
                    $_SESSION['output_message'] = 'Destaque atualizada com sucesso!';
                    $_SESSION['output_message_tipo'] = 'success';
                    return 1;
                } else {
                    $_SESSION['output_message'] = 'erro no upload da imagem!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            } else {
                $_SESSION['output_message'] = 'Não foi possível atualizar o destaque!';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        } else {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

    /**
     * Método para atualizar um destaque
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (!empty($this->destaque->getTitulo())) { 
            if(empty($_FILES['imagem']['name'])) {              
                if($this->updateItem($this->destaque, $id)) {
                    $_SESSION['output_message'] = 'Destaque atualizada com sucesso!';
                    $_SESSION['output_message_tipo'] = 'success';
                    return 1;
                } else {
                    $_SESSION['output_message'] = 'Não foi possível atualizar o destaque!';
                    $_SESSION['output_message_tipo'] = 'danger';
                    return 0;
                }
            }else {
                $this->updateImagem($data, $id);
            }    
        } else {
            $_SESSION['output_message'] = 'Destaque não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }


    /**
     * Método que deleta os destaque
     * @param $id
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload.$imagem['imagem']);
            $_SESSION['output_message'] = 'Destaque deletado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        } else {
            $_SESSION['output_message'] = 'Erro ao deletar destaque!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

}