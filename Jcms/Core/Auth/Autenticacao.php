<?php

namespace Jcms\Core\Auth;

use Jcms\Core\Models\Dao\ConfiguracoesDAO;

class Autenticacao extends ConfiguracoesDAO
{
    /**
     * mensagem de erro de email, caso esteja
     * @var bool
     */
    private $mEmail = false;

    /**
     * @param $email
     * @param $senha
     */
    public function login($email, $senha)
    {
        if (!$this->verificaSenha($senha) || !$this->verificaUsuario($email)) {
            $_SESSION['form']['email'] = $email;
            $_SESSION['output_message'] = "Login e senha não conferem";
            return 0;
        }

        unset($_SESSION['form']['email']);
        $_SESSION['backend']['user_logged'] = true;
        $_SESSION['backend']['user_loggin_timestamp'] = time();
        $_SESSION['action-sucess'] = true;
        $_SESSION['instances']['backend']['user'] = 'Administrador';
        return 1;
    }

    /**
     * @param $senha
     * @return int
     */
    public function verificaSenha($senha)
    {
        $this->addFilter(array("name", "=", 'PASSWORD_CMS'));
        $password = $this->listItems(0, 1);
        if (md5($senha) === $password[0]['value'])
            return 1;
        return 0;
    }

    /**
     * @param $us
     * @return int
     */
    public function verificaUsuario($us)
    {
        if ($us != USUARIO)
            return 0;

        return 1;
    }

    /**
     * @return string
     */
    public function logoff()
    {
        session_destroy();
        $_SESSION['backend']['user_logged'] = false;
        return 1;
    }


    /**
     * @return int
     */
    public function verificaSessao()
    {
        if (!isset($_SESSION['backend']['user_logged']) && !$_SESSION['backend']['user_logged'])
            return 0;
        return 1;
    }
}