<?php

namespace Jcms\Core\Models;

class Institucional {

	private $conteudoID;
	private $nome;
	private $titulo;
    private $subTitulo;
	private $chamada;
	private $conteudo;
    private $imagemDestaque;
    /*
        getters and setters
    */
    public function __construct($id=null) {
        if (!empty($id))
        	$this->conteudoID=$id;
    }

	public function getConteudoID() { return $this->conteudoID; }
	public function getNome() { return $this->nome; }
	public function getTitulo() { return $this->titulo; }
    public function getSubTitulo() { return $this->subTitulo; }
	public function getChamada() { return $this->chamada; }
	public function getConteudo() { return $this->conteudo; }
    public function getImagemDestaque() { return $this->imagemDestaque; }

	public function setConteudoID($conteudoID) { $this->conteudoID=$conteudoID; }
	public function setNome($nome) { $this->nome=$nome; }
	public function setTitulo($titulo) { $this->titulo=$titulo; }
    public function setSubTitulo($titulo) { $this->subTitulo=$titulo; }
	public function setChamada($chamada) { $this->chamada=$chamada; }
	public function setConteudo($conteudo) { $this->conteudo=$conteudo; }
    public function setImagemDestaque($imagem) { $this->imagemDestaque=$imagem; }

}