<?php

namespace Jcms\Core\Models;

use PDO;
/**
 * Classe que faz a conexao com o banco de dados
 */
class Database extends PDO {

    private static $con = NULL;

    private $DBHost = DB_HOST;
    private $DBName = DB_NAME;
    private $DBUser = DB_USER;
    private $DBPass = DB_PASS;
    private $DBType = BD_TYPE;

    public function __construct() {
        try {            
            self::$con = parent::__construct(
                $this->DBType.':host='.$this->DBHost.';dbname='.$this->DBName, 
                $this->DBUser, 
                $this->DBPass
            );
            parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
           die("Ocorreu algum erro na conexao, mensagem: ". $e->getMessage());
        }
    }

    public function conn() {
        if(!self::$con)
            self::$con = new Database();

        return self::$con;
    }

}

?>