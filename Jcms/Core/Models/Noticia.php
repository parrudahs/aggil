<?php

namespace Jcms\Core\Models;

class Noticia
{

    private $noticiaID;
    private $url;
    private $categoriaID;
    private $partidaID;
    private $titulo;
    private $resumo;
    private $conteudo;
    private $dataNoticia;
    private $publicada;
    private $destaque;
    private $imagemDestaque;
    private $videoURL;

    public function __construct($id=null, $url=null) {
        if (!empty($id))
            $this->noticiaID=$id;

        if (!empty($url))
            $this->url=$url;
    }

    /*
        getters and setters
    */
    public function getNoticiaID() { return $this->noticiaID; }
    public function getURL() { return $this->url; }
    public function getCategoriaID() { return $this->categoriaID; }
    public function getPartidaID() { return $this->partidaID; }
    public function getTitulo() { return $this->titulo; }
    public function getResumo() { return $this->resumo; }
    public function getConteudo() { return $this->conteudo; }
    public function getDataNoticia() { return $this->dataNoticia; }
    public function getPublicada() { return $this->publicada; }
    public function getDestaque() { return $this->destaque; }
    public function getImagemDestaque() { return $this->imagemDestaque; }
    public function getVideoURL() { return $this->videoURL; }

    public function setNoticiaID($id) { $this->noticiaID=$id; }
    public function setURL($url) { $this->url=$url; }
    public function setCategoriaID($categoriaID) { $this->categoriaID=$categoriaID; }
    public function setPartidaID($id) { $this->partidaID=$id; }
    public function setTitulo($titulo) { $this->titulo=$titulo; }
    public function setResumo($resumo) { $this->resumo=$resumo; }
    public function setConteudo($conteudo) { $this->conteudo=$conteudo; }
    public function setDataNoticia($dataNoticia) { $this->dataNoticia=$dataNoticia; }
    public function setPublicada($publicada) { $this->publicada=$publicada; }
    public function setDestaque($destaque) { $this->destaque=$destaque; }
    public function setImagemDestaque($img) { $this->imagemDestaque=$img; }
    public function setVideoURL($url) { $this->videoURL=$url; }

}

?>
