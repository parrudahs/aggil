<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Noticia;

class NoticiasDAO extends DAO {

    private $tbName = "tb_noticias";
    private $tableID = "noticia_id";
    private $attributes = array();

    private $rowCount;

    public $sorterBy = array();
    public $filterBy = array();

    /**
     * @param Noticia $data
     * @return int
     */
    public function insertItem(Noticia $data)
    {
        $this->attributes['url'] = $data->getURL();
        $this->attributes['categoria_id'] =  $data->getCategoriaID();
        $this->attributes['titulo'] = $data->getTitulo();
        $this->attributes['resumo'] = $data->getResumo();
        $this->attributes['conteudo'] = $data->getConteudo();
        $this->attributes['data_noticia'] = $data->getDataNoticia();
        $this->attributes['video_url'] = $data->getVideoURL();
        $this->attributes['publicada'] =  $data->getPublicada();
        $this->attributes['destaque'] =  $data->getDestaque();
        $this->attributes['imagem_destaque'] =  $data->getImagemDestaque();
        

        return $this->insertDAO($this->tbName, $this->attributes);
    }

    /**
     * @param Noticia $data
     * @param $id
     * @return int
     */
    public function updateItem(Noticia $data, $id)
    {
        $this->attributes['url'] = $data->getURL();
        $this->attributes['categoria_id'] =  $data->getCategoriaID();
        $this->attributes['titulo'] = $data->getTitulo();
        $this->attributes['resumo'] = $data->getResumo();
        $this->attributes['conteudo'] = $data->getConteudo();
        $this->attributes['data_noticia'] = $data->getDataNoticia();
        $this->attributes['video_url'] = $data->getVideoURL();
        $this->attributes['publicada'] =  $data->getPublicada();
        $this->attributes['destaque'] =  $data->getDestaque();
        $this->attributes['imagem_destaque'] =  $data->getImagemDestaque();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
    }
    
    /**
     * @param $acao
     * @param $id
     * @return int
     */
    public function publicarDestacar($acao, $id)
    {
        switch ($acao) {
            case 1:
                $this->attributes['publicada'] = 1;
                break;
            case 2:
                $this->attributes['destaque'] = 1;
                break;
            case 3:
                $this->attributes['publicada'] = 0;
                break;
            case 4:
                $this->attributes['destaque'] = 0;
                break;
            case 5:
                $this->attributes['publicada'] = 0;
                $this->attributes['destaque'] = 0;
                break;
        }

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return mixed
     */
    public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }

    /**
     * @param Noticia $data
     * @param $id
     * @return int
     */
    public function atualizarImagem(Noticia $data, $id)
    {
        $this->attributes['imagem_destaque'] = $data->getImagemDestaque();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $param
     */
    public function addSorter($param)
    {
        array_push($this->sorterBy,$param);
    }

    /**
     * @param $param
     */
    public function addFilter($param)
    {
        array_push($this->filterBy,$param);
    }

    /**
     * @param $param
    */
    public function clearFilter()
    {
        $this->filterBy = array();
    }

    /**
     * @param $param
    */
    public function clearSorter()
    {
        $this->sorterBy = array();
    }


    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }

}


?>