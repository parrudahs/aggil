<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Dao\DAO;
use Jcms\Core\Models\Institucional;

class InstitucionaisDAO extends DAO
{

    private $tbName = "tb_institucionais";
    private $tableID = "conteudo_id";
    private $attributes = array();

    private $rowCount;

    public $sorterBy = array();
    public $filterBy = array();

    /**
     * @param array $data
     * @return int
     */
    public function insertItem(Institucional $data)
    {
        $this->attributes['nome'] = $data->getNome();
        $this->attributes['titulo'] =  $data->getTitulo();
        $this->attributes['sub_titulo'] = $data->getSubTitulo();
        $this->attributes['chamada'] = $data->getChamada();
        $this->attributes['conteudo'] = $data->getConteudo();

        return $this->insertDAO($this->tbName, $this->attributes);
    }

    /**
     * @param array $data
     * @param $id
     * @return int
     */
    public function updateItem(Institucional $data, $id)
    {

        $this->attributes['titulo'] =  $data->getTitulo();
        $this->attributes['sub_titulo'] = $data->getSubTitulo();
        $this->attributes['chamada'] = $data->getChamada();
        $this->attributes['conteudo'] = $data->getConteudo();
        $this->attributes['imagem_destaque'] = $data->getImagemDestaque();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
    }

    /**
     * @param Destaque $data
     * @param $id
     * @return mixed
     */
    public function atualizarImagem(Institucional $data, $id)
    {
        $this->attributes['imagem_destaque'] =  $data->getImagemDestaque();
        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $sorterBy
     * @param $filterBy
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return mixed
     */
    public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }

    /**
     * @param $param
     */
    public function addSorter($param)
    {
        array_push($this->sorterBy,$param);
    }

    /**
     * @param $param
     */
    public function clearFilter()
    {
        $this->filterBy = array();
    }

    /**
     * @param $param
     */
    public function addFilter($param)
    {
        array_push($this->filterBy,$param);
    }

    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }
    
}