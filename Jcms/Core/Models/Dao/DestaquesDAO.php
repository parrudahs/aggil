<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Dao\DAO;
use Jcms\Core\Models\Destaque;

class DestaquesDAO extends DAO
{

    /**
     * Nome da tabela destaque
     * @var string
     */
    private $tbName = "tb_destaques";
    protected $tableID = "destaque_id";
    private $attributes = array();

    private $rowCount;

    public $sorterBy = array();
    public $filterBy = array();

    /**
     * método para inserção de um novo item destaque
     * @param Destaque $data
     * @return int
     */
    public function insertItem(Destaque $data)
    {
        $this->attributes['titulo'] = $data->getTitulo();
        $this->attributes['ordem'] =  $data->getOrdem();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['imagem'] = $data->getImagem();
        $this->attributes['url'] = $data->getURL();
        $this->attributes['comportamento'] = $data->getComportamento();
        $this->attributes['timestamp_cadastro'] = 'unix_timestamp';
        $this->attributes['publicado'] =  $data->getPublicado();

        return $this->insertDAO($this->tbName, $this->attributes);
    }

    /**
     * Método para atualização de um destaque
     * @param Destaque $data
     * @param $id
     * @return int
     */
    public function updateItem(Destaque $data, $id)
    {
        $this->attributes['titulo'] = $data->getTitulo();
        $this->attributes['ordem'] =  $data->getOrdem();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['imagem'] = $data->getImagem();
        $this->attributes['url'] = $data->getURL();
        $this->attributes['comportamento'] = $data->getComportamento();
        $this->attributes['publicado'] =  $data->getPublicado();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * Método da para deletar um destaque
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
    }

    /**
     * Listagem do(s) destaque(s)
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return array|mixed
     */
    public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }

    /**
     * Adiciona ordem
     * @param $param
     */
    protected function addSorter($param)
    {
    	array_push($this->sorterBy, $param);
    }

    protected function clearSorter()
    {
        $this->sorterBy = array();
    }

    /**
     * @param $param
     */
    public function clearFilter()
    {
        $this->filterBy = array();
    }
    
    /**
     * adiciona filtros
     * @param $param
     */
    protected function addFilter($param)
    {
    	array_push($this->filterBy, $param);
    }

    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }

}