<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Foto;

class FotosDAO extends DAO
{

    private $tbName="tb_fotos";
    private $tableID = "foto_id";
    private $attributes = array();

    private $rowCount;

    public $sorterBy=array();
    public $filterBy=array();

    /**
     * @param Foto $data
     * @return int
     */
    public function insertItem(Foto $data)
    {
        $this->attributes['galeria_id'] = $data->getGaleriaID();
        $this->attributes['creditos'] =  $data->getCreditos();
        $this->attributes['arquivo'] = $data->getArquivo();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['capa'] = $data->getCapa();
        $this->attributes['publicada'] = $data->getPublicada();


        return $this->insertDAO($this->tbName, $this->attributes);
    }

    /**
     * @param Foto $data
     * @param $id
     * @return int
     */
    public function updateItem(Foto $data, $id)
    {
        $this->attributes['creditos'] =  $data->getCreditos();
        $this->attributes['arquivo'] = $data->getArquivo();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['capa'] = $data->getCapa();
        $this->attributes['publicada'] = $data->getPublicada();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
    }

    /**
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return array|mixed
     */
    public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }

    /**
     * @param $param
     */
    public function addSorter($param)
    {
        array_push($this->sorterBy,$param);
    }

    /**
     * @param $param
     */
    public function clearFilter()
    {
        $this->filterBy = array();
    }
    
    /**
     * @param $param
     */
    public function addFilter($param)
    {
        array_push($this->filterBy,$param);
    }

    /**
     * @return mixed
    */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }
}    

?>