<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Dao\DAO;
use Jcms\Core\Models\Categoria;

class CategoriasDAO extends DAO
{
	private $tbName = "tb_categorias";
    private $tableID = "categoria_id";
    private $attributes = array();

	public $sorterBy = array();
	public $filterBy = array();



    /**
     * Método para inserir uma nova categoria
     * @param Categoria $data
     * @return int
     */
	public function insertItem(Categoria $data)
    {
        $this->attributes['categoria_pai_id'] = $data->getCategoriaPaiID();
        $this->attributes['nome'] =  $data->getNome();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['ordem'] = $data->getOrdem();

        return $this->insertDAO($this->tbName, $this->attributes);
	}

    /**
     * Método para atualizar a categoria
     * @param Categoria $data
     * @param $id
     * @return int
     */
	public function updateItem(Categoria $data, $id)
    {
        $this->attributes['categoria_pai_id'] = $data->getCategoriaPaiID();
        $this->attributes['nome'] =  $data->getNome();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['ordem'] = $data->getOrdem();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
	}

    /**
     * Método para deletar Categoria
     * @param $id
     * @return int|mixed
     */
	public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
	}

    /**
     * Método de listagem de categoria
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return array|mixed
     */
	public function listItems($rowIndexIni, $rowIndexEnd)
	{
		return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
	}

	public function addSorter($param)
	{
		array_push($this->sorterBy,$param);
	}

	public function addFilter($param)
	{
		array_push($this->filterBy,$param);
	}

}

?>