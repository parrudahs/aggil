<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Galeria;

class GaleriasDAO extends DAO
{

    private $tbName = "tb_galerias";
    private $tableID = "galeria_id";
    private $attributes = array();

    private $rowCount;

	public  $sorterBy=array();
	public  $filterBy=array();

    /**
     * @param Galeria $data
     * @return int
     */
	public function insertItem(Galeria $data)
    {
        $this->attributes['categoria_id'] = $data->getCategoriaID();
        $this->attributes['titulo'] =  $data->getTitulo();
        $this->attributes['data_fotos'] = $data->getDatafotos();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['texto'] = $data->getTexto();
        $this->attributes['destaque'] = $data->getDestaque();
        $this->attributes['noticia'] = $data->getNoticia();
        $this->attributes['publicada'] = $data->getPublicada();
        $this->attributes['timestamp'] = 'unix_timestamp';

        return $this->insertDAO($this->tbName, $this->attributes);

	}

    /**
     * @param Galeria $data
     * @param $id
     * @return int
     */
	public function updateItem(Galeria $data, $id)
    {
        $this->attributes['categoria_id'] = $data->getCategoriaID();
        $this->attributes['titulo'] =  $data->getTitulo();
        $this->attributes['data_fotos'] = $data->getDatafotos();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['texto'] = $data->getTexto();
        $this->attributes['destaque'] = $data->getDestaque();
        $this->attributes['noticia'] = $data->getNoticia();
        $this->attributes['publicada'] = $data->getPublicada();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
	}

    /**
     * deleta galeria
     * @param $id
     * @return int
     */
	public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
	}

    /**
     * deleta fotos da galeria
     * @param $id
     * @return int
     */
    public function deleteFotos($id)
    {
        return $this->deleteDAO('tb_fotos', 'galeria_id', $id);
    }

    /**
     * Lista todas as fotos da galeria
     */
    public function listFotos($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO('tb_fotos', $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO('tb_fotos', $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }

    /**
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return array|mixed
     */
	public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
	}

    /**
     * @param $param
     */
	public function addSorter($param)
    {
        array_push($this->sorterBy,$param);
	}

    /**
     * @param $param
     */
    public function clearFilter()
    {
        $this->filterBy = array();
    }
    
    /**
     * @param $param
     */
	public function addFilter($param)
    {
        array_push($this->filterBy,$param);
	}

    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    protected function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }
}


?>