<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Configuracao;
use Jcms\Core\Models\Dao\DAO;

class ConfiguracoesDAO extends DAO
{

    private $tbName = "tb_configuracoes";
    private $tableID = "config_id";
    private $attributes = array();

    private $rowCount;

    public $sorterBy = array();
    public $filterBy = array();

    /**
     * Método para inserção
     * @param Configuracao $data
     * @return mixed
     */
    public function insertItem(Configuracao $data) 
    {
        $this->attributes['name'] = $data->getName();
        $this->attributes['value'] =  $data->getValue();
    
        return $this->insertDAO($this->tbName, $this->attributes);
    }

    /**
     * @param Configuracao $data
     * @param $id
     * @return mixed
     */
    public function updateItem(Configuracao $data, $id)
    {
        $this->attributes['name'] = $data->getName();
        $this->attributes['value'] =  $data->getValue();

        return $this->updateDAO($this->tbName, $this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tbName, $this->tableID, $id);
    }

    /**
     * @param $sorterBy
     * @param $filterBy
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return mixed
     */
    public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->tbName, $this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }

    /**
     * @param $param
     */
    public function addSorter($param)
    {
        array_push($this->sorterBy,$param);
    }

    /**
     * @param $param
     */
    public function addFilter($param)
    {
        array_push($this->filterBy,$param);
    }


    /**
     * @param $param
     */
    public function clearFilter()
    {
        $this->filterBy = array();
    }

    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }

}    
    

?>