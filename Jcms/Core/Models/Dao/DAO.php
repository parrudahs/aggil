<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Database;
use Jcms\Core\Models\Destaque;

abstract class DAO extends Database {

    /**
     * @param $tableName
     * @param $sorterBy
     * @param $filterBy
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @param null $where_clause
     * @param bool $rowCount
     * @return array|int|mixed
     */
    public function listDAO($tableName, $sorterBy, $filterBy, $rowIndexIni, $rowIndexEnd, $rowCount = null, $where_clause = null) {
        try {
            $sorter = $this->sorter($sorterBy);

            if (empty($where_clause))
                $filter = $this->filtros($filterBy);
            else
                $filter = $where_clause;

            $rowIndexIni = (int) $rowIndexIni;
            $rowIndexEnd = (int) $rowIndexEnd;

            if (isset($rowIndexEnd) && $rowIndexEnd > 0)
                $limit = "LIMIT " . $rowIndexIni . "," . $rowIndexEnd;

            $sql = "SELECT * FROM " . $tableName . " " . (!empty($filter) ? $filter : null) . " " . (!empty($sorter) ? $sorter : null) . " " . (!empty($limit) ? $limit : null);
            //echo $sql;
            $stmt = parent::conn()->prepare($sql);

            foreach ($filterBy as $f) {
                $stmt->bindValue(":" . $f[0], $f[2]);
            }

            $stmt->execute();

            if ($rowCount == true)
                return $stmt->rowCount();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);

        } catch (\PDOException $e) {
            die("Erro ao selecionar : " . $e->getMessage());
        }
    }

    /**
     * Método de Inserção do DAO
     * @param $tbName
     * @param array $dados
     * @return int
     */
    public function insertDAO($tbName, array $dados) {
        try {
            $sql = "INSERT INTO " . $tbName;
            $colunas = '';
            $values = '';
            foreach ($dados as $k => $v) {
                $colunas .= $k . ",";
                if ($v == 'unix_timestamp')
                    $values .= "UNIX_TIMESTAMP(),";
                else
                    $values .= ":" . $k . ",";
            }
            $colunas = substr($colunas, 0, -1);
            $values = substr($values, 0, -1);

            $sql .= " (" . $colunas . ") VALUES( " . $values . ")";

            $stmt = parent::conn()->prepare($sql);
            foreach ($dados as $k => $v) {
                if ($v != 'unix_timestamp')
                    $stmt->bindValue(":" . $k, $v);
            }
            if ($stmt->execute())
                return 1;
        } catch (\PDOException $e) {
            die("erro:" . $e->getMessage());
        }
        return 0;
    }

    /**
     * Método de update do DAO
     * @param $tbName
     * @param array $dados
     * @param $campo
     * @param $id
     * @return int
     */
    public function updateDAO($tbName, array $dados, $campo, $id) {
        try {
            $colunas = '';
            foreach ($dados as $k => $v) {
                $colunas .= $k . "= :" . $k . ",";
            }
            $colunas = substr($colunas, 0, -1);
            $sql = "UPDATE " . $tbName . " SET " . $colunas . " WHERE " . $campo . "= :" . $campo;
            $stmt = parent::conn()->prepare($sql);
            foreach ($dados as $k => $v) {
                if ($v != 'unix_timestamp')
                    $stmt->bindValue(":" . $k, $v);
            }
            $stmt->bindValue(":" . $campo, $id);
            if ($stmt->execute())
                return 1;
        } catch (\PDOException $e) {
            die("erro:" . $e->getMessage());
        }

        return 0;
    }

    /**
     * Método de delete do DAO
     * @param $tbName
     * @param $campo
     * @param $value
     * @return int
     */
    public function deleteDAO($tbName, $campo, $value) {
        try {
            if (!empty($campo) && !empty($value)) {
                $sql = "DELETE FROM " . $tbName . " WHERE " . $campo . " = ? ";
                $stmt = parent::conn()->prepare($sql);
                $stmt->bindParam(1, $value);
                if ($stmt->execute())
                    return 1;
            }
            return 0;
        } catch (\PDOException $e) {
            die("erro:" . $e->getMessage());
        }
        return 0;
    }

    private function sorter($sorterBy) {
        $sorter = null;
        if (is_array($sorterBy) && !empty($sorterBy)) {
            $sorter = "ORDER BY ";
            $num = sizeof($sorterBy);
            foreach ($sorterBy as $k => $s) {
                if ($k == ($num - 1))
                    $sorter .= $s[0] . " " . $s[1] . " ";
                else
                    $sorter .= $s[0] . " " . $s[1] . ", ";
            }
        }
        return $sorter;
    }

    private function filtros($filterBy) {
        if (is_array($filterBy) && !empty($filterBy)) {
            $filter = " WHERE 1";
            foreach ($filterBy as $f) {
                $filter .= " AND " . $f[0] . " " . $f[1] . " :" . $f[0];
            }
            return $filter;
        }
        return null;
    }

}
