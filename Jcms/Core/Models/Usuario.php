<?php

namespace Jcms\Core\Models;

class Usuario {

	private $usuarioID;
	private $email;
	private $nomeCompleto;

    /*
        getters and setters
    */

    public function __construct($id=null) {
        if (!empty($id))
        $this->usuarioID=$id;
    }

	public function getUsuarioID() { return $this->usuarioID; }
	public function getEmail() { return $this->email; }
	public function getNomeCompleto() { return $this->nomeCompleto; }

	public function setUsuarioID($usuarioID) { $this->usuarioID=$usuarioID; }
	public function setEmail($email) { $this->email=$email; }
	public function setNomeCompleto($nomeCompleto) { $this->nomeCompleto=$nomeCompleto; }
    
    public function gerarKey() {

        $nunsRand = null;
        for ($i=0; $i < 9; $i++) {
            $nunsRand .= mt_rand(0, 9);
        }
        $key = md5($nunsRand);
        return $key;
    }
    
    public function gerarSenha() {

        $vogais = "aeiouy";
        $consoantes = "bcdfghjklmnpqrstvxz";

        $senha = "";

        for ($i=0;$i<8;$i++) {
            if (($i%2)==0) {
               $index = mt_rand(0,18);
               $senha .= $consoantes[$index];
            }
            else {
                if (mt_rand(0,1)) {
                    $senha .= mt_rand(0,9);
                }
                else {
                    $index = mt_rand(0,5);
                    $senha .= $vogais[$index];
                }
            }

        }
        return $senha;
    }
        
    public function isAuth($face) {
        if (isset($_SESSION[$face]['user_logged']) && (isset($_SESSION[$face]['user_id']) && !empty($_SESSION[$face]['user_id'])) && (isset($_SESSION[$face]['user_loggin_timestamp']) && !empty($_SESSION[$face]['user_loggin_timestamp'])))
            return 1;
        else
            return 0;
    }
        
    public function logoff($face) {
        unset($_SESSION[$face]['user_logged']);
        unset($_SESSION[$face]['user_id']);
        unset($_SESSION[$face]['user_loggin_timestamp']);
    }
    
    }

?>