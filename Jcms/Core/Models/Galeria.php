<?php

namespace Jcms\Core\Models;

class Galeria
{
	private $galeriaID;
    private $categoriaID;
    private $titulo;
	private $datafotos;
    private $descricao;
	private $texto;
	private $publicada;
    private $destaque;
    private $noticia;
    private $timestamp;
    /*
        getters and setters
    */
    public function __construct($id=null) {
        if (!empty($id))
            $this->galeriaID=$id;
    }

	public function getGaleriaID() { return $this->galeriaID; }
    public function getCategoriaID() { return $this->categoriaID; }
    public function getTitulo() { return $this->titulo; }
	public function getDatafotos() { return $this->datafotos; }
    public function getDescricao() { return $this->descricao; }
	public function getTexto() { return $this->texto; }
	public function getPublicada() { return $this->publicada; }
    public function getDestaque() { return $this->destaque; }
    public function getNoticia() { return $this->noticia; }
    public function getTimestamp() { return $this->timestamp; }

	public function setGaleriaID($galeriaID) { $this->galeriaID=$galeriaID; }
    public function setCategoriaID($categoriaID) { $this->categoriaID=$categoriaID; }
    public function setTitulo($titulo) { $this->titulo=$titulo; }
	public function setDatafotos($datafotos) { $this->datafotos=$datafotos; }
    public function setDescricao($descricao) { $this->descricao=$descricao; }
	public function setTexto($texto) { $this->texto=$texto; }
	public function setPublicada($publicada) { $this->publicada=$publicada; }
    public function setDestaque($destaque) { $this->destaque=$destaque; }
    public function setNoticia($noticia) { $this->noticia=$noticia; }
    public function setTimestamp($time) { $this->timestamp=$time; }
        
}

?>