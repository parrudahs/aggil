<?php

namespace Jcms\Core\Models;

class Categoria {

	private $categoriaID;
    private $categoriaPaiID;
    private $ordem;
	private $nome;
	private $descricao;

	public function __construct($id=null) {
		if (!empty($id))
			$this->categoriaID=$id;
	}

    /*
        getters and setters
    */
	public function getCategoriaID() { return $this->categoriaID; }
    public function getCategoriaPaiID() { return $this->categoriaPaiID; }
	public function getNome() { return $this->nome; }
	public function getDescricao() { return $this->descricao; }
    public function getOrdem() { return $this->ordem; }

	public function setCategoriaID($categoriaID) { $this->categoriaID=$categoriaID; }
    public function setCategoriaPaiID($categoriaPaiID) { $this->categoriaPaiID=$categoriaPaiID; }
	public function setNome($nome) { $this->nome=$nome; }
	public function setDescricao($descricao) { $this->descricao=$descricao; }
    public function setOrdem($ordem) { $this->ordem=$ordem;  }

}

?>