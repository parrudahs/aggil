<?php

namespace Jcms\Core\Models;

class Destaque {

    private $destaqueID;
    private $titulo;
    private $ordem;
    private $descricao;
    private $imagem;
    private $url;
    private $comportamento;
    private $timestampCadastro;
    private $publicado;

    /*
        getters and setters
    */

    public function __construct($id=null) {
        if (!empty($id))
            $this->destaqueID=$id;
    }

    public function getDestaqueID() { return $this->destaqueID; }
    public function getTitulo() { return $this->titulo; }
    public function getOrdem() { return $this->ordem; }
    public function getDescricao() { return $this->descricao; }
    public function getImagem() { return $this->imagem; }
    public function getURL() { return $this->url; }
    public function getComportamento() { return $this->comportamento; }
    public function getTimestampCadastro() { return $this->timestampCadastro; }
    public function getPublicado() { return $this->publicado; }

    public function setDestaqueID($destaqueID) { $this->destaqueID=$destaqueID; }
    public function setTitulo($titulo) { $this->titulo=$titulo; }
    public function setOrdem($ordem) { $this->ordem=$ordem; }
    public function setDescricao($descricao) { $this->descricao=$descricao; }
    public function setImagem($imagem) { $this->imagem=$imagem; }
    public function setURL($url) { $this->url=$url; }
    public function setComportamento($comportamento) { $this->comportamento=$comportamento; }
    public function setTimestampCadastro($timestampCadastro) { $this->timestampCadastro=$timestampCadastro; }
    public function setPublicado($publicado) { $this->publicado=$publicado; }

}