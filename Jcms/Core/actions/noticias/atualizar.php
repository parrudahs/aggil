<?php

$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');


$att = new Jcms\Core\Controllers\NoticiaController();
if(isset($_POST['atualizar'])){
    if($att->update($_POST, $urls[3])) {
        header('Location:'.BASE_URL.'admin/lista-noticias');
    } else {
        header('Location:'.BASE_URL.'admin/atualiza-noticia/'.$urls[3]);
    }
}

    /*set_time_limit(0);

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    
    $noticia = new Noticia($id);
    $partida_id=$_REQUEST['partida'];
    $titulo=$_REQUEST['titulo'];
    $url=Content::gerarURL($titulo);
    $resumo=$_REQUEST['resumo'];
    $conteudo=$_REQUEST['conteudo'];
    $dataNoticia=$_REQUEST['data-noticia'];
    $video_url=$_REQUEST['video-url'];
    $publicada=$_REQUEST['publicada'];
    $destaque=$_REQUEST['destaque'];

    $data=substr($dataNoticia,0,10);
    $hr=substr($dataNoticia,11,5);
    
    $dataNoticia=DataHr::toUnixTime($data, $hr);
    
    Forms::setFormName("frm-edit-noticia");
    
    $sucess = false;
    
    if (!empty($titulo) && !empty($conteudo)) {
    
        $noticia->setURL($url."-".date("dmYHi",$dataNoticia));
        $noticia->setCategoriaID($_REQUEST['categoria']);
        $noticia->setTitulo($titulo);
        $noticia->setResumo($resumo);
        $noticia->setConteudo($conteudo);
        $noticia->setDataNoticia($dataNoticia);
        $noticia->setVideoURL($video_url);
        $noticia->setPublicada($publicada);
        $noticia->setDestaque($destaque);
        
        NoticiasDAO::setObject($noticia);
        NoticiasDAO::DBConnection();
        
        if (NoticiasDAO::updateItem()) {
            
            if (isset($_REQUEST['sem-imagem']) && ($_REQUEST['sem-imagem']==1)) {
                if (!empty($_REQUEST['imagem-atual']) && is_file("../../../imagens/noticias/".$_REQUEST['imagem-atual'])) {
                    $ftp = new FTP($ftpHost, $ftpUserName, $ftpUserPassword);
                    $ftp->ftpConnection();
                    $ftp->deleteFile(PATH_SITE."/imagens/noticias/".$_REQUEST['imagem-atual']);
                    $ftp->deleteFile(PATH_SITE."/imagens/noticias/400x300-".$_REQUEST['imagem-atual']);
                    $ftp->deleteFile(PATH_SITE."/imagens/noticias/140x90-".$_REQUEST['imagem-atual']);
                    $ftp->close();
                }
                $noticia->setImagemDestaque("");
            }
            else if (!empty($_FILES['imagem-destaque']['name'])) {
                $image = $_FILES['imagem-destaque'];
                if (File::isImage($image['name'])) {
                    $ftp = new FTP($ftpHost, $ftpUserName, $ftpUserPassword);
                    $ftp->ftpConnection();                     
                    
                    $destaques_image_path = PATH_SITE."/imagens/noticias/";
                    $new_file_name = $noticia->getNoticiaID()."-destaque.".File::extension($image['name']);
                    
                    if ($ftp->sendFile($destaques_image_path.$new_file_name, $image['tmp_name'])) {                        
                        $imagem_atleta = new Image($new_file_name,"../../../imagens/noticias");
                        $imagem_atleta->resizeImage(1200,900,1,$new_file_name,"../../../imagens/noticias");
                        $imagem_atleta->resizeImage(400,300,1,"400x300-".$new_file_name,"../../../imagens/noticias");
                        $imagem_atleta->resizeImage(140,90,1,"140x90-".$new_file_name,"../../../imagens/noticias");
                        $noticia->setImagemDestaque($new_file_name);
                    }
                    
                    $ftp->close();                
                }
            }
            else
                $noticia->setImagemDestaque($_REQUEST['imagem-atual']);
                
            NoticiasDAO::atualizarImagem();
            
            $outputMessage="<p class='Jcms-msg-ok'>Noticia atualizada com sucesso!</p>";
            $sucess = true;
            
        }
        else
            $outputMessage="<p class='Jcms-msg-error'>N�o foi poss�vel atualizar a not�cia!<br/>Error: ".$noticia->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='Jcms-msg-error'>Not�cia n�o atualizada!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage)
*/

?>