<?php
$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

$cad = new Jcms\Core\Controllers\NoticiaController();
if(isset($_POST['cadastrar'])){
    if($cad->create($_POST)) {
        header('Location:'.BASE_URL.'admin/lista-noticias');
    } else {
        header('Location:'.BASE_URL.'admin/cadastro-noticia');
    }
}

    /*set_time_limit(0);

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    $titulo=$_REQUEST['titulo'];
    $partida_id=$_REQUEST['partida'];
    $url=Content::gerarURL($titulo);
    $resumo=$_REQUEST['resumo'];
    $conteudo=$_REQUEST['conteudo'];
    $dataNoticia=$_REQUEST['data-noticia'];
    $video_url=$_REQUEST['video-url'];
    $publicada=$_REQUEST['publicada'];
    $destaque=$_REQUEST['destaque'];

    $data=substr($dataNoticia,0,10);
    $hr=substr($dataNoticia,11,5);
    
    $dataNoticia=DataHr::toUnixTime($data, $hr);
    
    Forms::setFormName("frm-add-noticia");
    
    $sucess = false;
    
    if (!empty($titulo) && !empty($conteudo)) {
    
        $noticia = new Noticia();
        $noticia->setURL($url."-".date("dmYHi",$dataNoticia));
        $noticia->setCategoriaID($_REQUEST['categoria']);
        $noticia->setPartidaID($partida_id);
        $noticia->setTitulo($titulo);
        $noticia->setResumo($resumo);
        $noticia->setConteudo($conteudo);
        $noticia->setDataNoticia($dataNoticia);
        $noticia->setVideoURL($video_url);
        $noticia->setPublicada($publicada);
        $noticia->setDestaque($destaque);
        
        NoticiasDAO::setObject($noticia);
        NoticiasDAO::DBConnection();
        
        if (NoticiasDAO::insertItem()) {
            $outputMessage="<p class='Jcms-msg-ok'>Noticia cadastrada com sucesso!</p>";
            $sucess = true;
            
            if (!empty($_FILES['imagem-destaque'])) {
                $image = $_FILES['imagem-destaque'];
                if (File::isImage($image['name'])) {                    
                    $ftp = new FTP($ftpHost, $ftpUserName, $ftpUserPassword);
                    
                    if ($ftp->ftpConnection()) {

                        $destaques_image_path = PATH_SITE."/imagens/noticias/";
                        $new_file_name = $noticia->getLastInsertedID()."-destaque.".File::extension($image['name']);
                        
                        if ($ftp->sendFile($destaques_image_path.$new_file_name, $image['tmp_name'])) {
                            
                            $imagem_atleta = new Image($new_file_name,"../../../imagens/noticias");
                            $imagem_atleta->resizeImage(1200,900,1,$new_file_name,"../../../imagens/noticias");
                            $imagem_atleta->resizeImage(400,300,1,"400x300-".$new_file_name,"../../../imagens/noticias");
                            $imagem_atleta->resizeImage(140,90,1,"140x90-".$new_file_name,"../../../imagens/noticias");
                            
                            $noticia->setNoticiaID($noticia->getLastInsertedID());
                            $noticia->setImagemDestaque($new_file_name);
                            NoticiasDAO::atualizarImagem();
                        }
                        
                        $ftp->close();
                    }
                    else
                        print("<br/>n�o foi poss�vel conectar no servidor ftp");
                }
            }
            
        }
        else
            $outputMessage="<p class='Jcms-msg-error'>N�o foi poss�vel cadastrar a not�cia!<br/>Error: ".$noticia->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='Jcms-msg-error'>Not�cia n�o cadastrada!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);        
*/

?>