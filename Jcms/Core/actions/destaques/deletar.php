<?php

$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

$destaque = new Jcms\Core\Controllers\DestaqueController();
if(isset($urls[2]) && $urls[2] == 'deletar'){
    if($destaque->delete($urls[3])) {
        header('Location:'.BASE_URL.'admin/lista-destaques');
    } else {
        header('Location:'.BASE_URL.'admin/lista-destaques');
    } 
}
?>