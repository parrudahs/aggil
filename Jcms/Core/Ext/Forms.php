<?php

namespace Jcms\Core\Ext;

class Forms {
    
    private static $formName;
    private static $formData;
    
    public static function setFormName($name) {
        self::$formName = $name;
    }
    
    public static function setFormData(&$data) {
        $_SESSION['forms'][self::$formName]['data'] = serialize($data);
    }
    
    public static function getFormData() {
        if (isset($_SESSION['forms'][self::$formName]['data'])) {
            self::$formData = unserialize($_SESSION['forms'][self::$formName]['data']);
            return 1;
        }
        else {
            self::$formData="";
            return 0;
        }
    }
    
    public static function unsetFormData() {
        unset($_SESSION['forms'][self::$formName]['data']);
    }
    
    public static function formDataSetted() {
        if (!empty(self::$formData))                
            return 1;
        else
            return 0;
    }
    
    public static function fieldValue($fieldName, $fieldValue) {
        if (self::formDataSetted()) {
            if (isset(self::$formData[$fieldName]))
                return self::$formData[$fieldName];
            else
                return $fieldValue;
        }
        else
            return $fieldValue;
    }
    
    public static function setFormToken($form_name) {
        $vogais = "aeiouy";
        $consoantes = "bcdfghjklmnpqrstvxz";
        
        $senha = "";
        
        for ($i=0;$i<12;$i++) {
            if (($i%2)==0) {
               $index = mt_rand(0,18);
               $senha .= $consoantes[$index];
            }
            else {
                if (mt_rand(0,1)) {
                    $senha .= mt_rand(0,9);
                }
                else {
                    $index = mt_rand(0,5);
                    $senha .= $vogais[$index];                        
                }
            }
            
        }
        
        $senha = sha1($senha);
        
        $_SESSION['forms'][$form_name]['token'] = $senha;
        
        return $senha;              
    }
    
    public static function getFormToken($form_name) {
        if (isset($_SESSION['forms'][$form_name]['token']) && !empty($_SESSION['forms'][$form_name]['token']))
            return $_SESSION['forms'][$form_name]['token'];
        else
            return 0;
    }
    
    public static function unsetFormToken($form_name) {
        if (isset($_SESSION['forms'][$form_name]['token']))
            unset($_SESSION['forms'][$form_name]['token']);
    }
    
    public static function status($status=null) {
        
        $form_name = self::$formName;
        
        if ($status===null) {
            if (isset($_SESSION['forms'][$form_name]['status']))
                return $_SESSION['forms'][$form_name]['status'];
            else
                return null;                
        }
        else
            $_SESSION['forms'][$form_name]['status']=($status ? 1 : 2);
        
        return null;
        
    }
    
    public static function resetStatus() {
        $form_name = self::$formName;
        unset($_SESSION['forms'][$form_name]['status']);
    }
    
    public static function setOutputMessage($message) {
        $form_name = self::$formName;
        $_SESSION['forms'][$form_name]['message']=$message;
    }
    
    public static function getOutputMessage() {
        $form_name = self::$formName;
        if (isset($_SESSION['forms'][$form_name])) {
            
            if (isset($_SESSION['forms'][$form_name]['message']) && !empty($_SESSION['forms'][$form_name]['message']))
                return $_SESSION['forms'][$form_name]['message'];
            else
                return 0;
            
        }
        else
            return 0;
    }
    
    public static function resetOutputMessage() {
        $form_name = self::$formName;
        $_SESSION['forms'][$form_name]['message']="";
    }
    
}

?>