<?php

namespace Jcms\Core\Ext;

//class com metodos que precisaram ser criados, relacionados a data e hora
class DataHr {

			//metodo para adicionar 'x' horas a uma determindada data
	public static function add_hora($hrUx, $hrAd) {
		$hrAd = ($hrAd*3600);
		$hrFim = ($hrUx + $hrAd);
		return $hrFim;
	}

	public static function mesText($mesnum) {
		switch($mesnum) {
			case 1: $mestext="Janeiro"; break;
			case 2: $mestext="Fevereiro"; break;
			case 3: $mestext="Março"; break;
			case 4: $mestext="Abril"; break;
			case 5: $mestext="Maio"; break;
			case 6: $mestext="Junho"; break;
			case 7: $mestext="Julho"; break;
			case 8: $mestext="Agosto"; break;
			case 9: $mestext="Setembro"; break;
			case 10: $mestext="Outubro"; break;
			case 11: $mestext="Novembro"; break;
			case 12: $mestext="Dezembro"; break;
		}
		return $mestext;
	}


	//transforma uma hora dada em segundos para o formato HH:mm
	public static function secToTime($timeSec) {
		$hrSec = floor($timeSec/3600);
		$hrSec = (strlen($hrSec) == 0) ? "0" . $hrSec : $hrSec;
		$minSec = ($timeSec%3600);
		$minSec = floor($minSec/60);
		$minSec = (strlen($minSec == 0)) ? "0" . $minSec : $minSec;
		$horaFim = $hrSec . ":" . $minSec;
		return $horaFim;
	}

	//transforma uma hora no formato HH:mm para segundos
	public static function timeToSec($strTime) {
		$hora = explode(":", $strTime);
		$hrSec = ($hora[0]*3600);
		$minSec = ($hora[1]*60);
		return ($hrSec+$minSec);
	}
	
	public static function secToMins($sec) {
		$mins = floor($sec/60); 
		$secs = ($sec%60);
		$strTime = ((strlen($mins)==1) ? "0".$mins : $mins) . ":" . ((strlen($secs)==1) ? "0".$secs : $secs);
		return $strTime;
}
	
	//metodo para tranformar string do tipo 'dd/mm/YYYY HH:ii' para UnixTimesTamp '1102478631'
	public static function toUnixTime($strData, $strHr) {
		$dataUx = substr($strData,6,4) . "-" . substr($strData,3,2) . "-" . substr($strData,0,2);
		$hrUx = ($strHr) ? self::timeToSec($strHr) : 0;
		$dataR = (strtotime($dataUx, 1) + $hrUx);
		return $dataR;
	}
	
	public static function toISO($strData, $strHr) {
		$dataUx = substr($strData,6,4) . "-" . substr($strData,3,2) . "-" . substr($strData,0,2);
		$dataISO  = $dataUx.($strHr ? " ".$strHr : null);
		return $dataISO;
	}
	
	public static function diaSemText($diaInt) {
		switch($diaInt) {
			case 0: $ditaText = "Domingo"; break;
			case 1: $ditaText = "Segunda-Feira"; break;
			case 2: $ditaText = "Terça-Feira"; break;
			case 3: $ditaText = "Quarta-Feira"; break;
			case 4: $ditaText = "Quinta-Feira"; break;
			case 5: $ditaText = "Sexta-Feira"; break;
			case 6: $ditaText = "Sábado"; break;
		}
		return $ditaText;
	}
	
	public static function idade($ux_datanasc) {
		return $ux_datanasc;
	}

	public static function idadeF($data) {
		$dia = (int) substr($data, 0, 2);
		$mes = (int) substr($data, 2, 2);
		$ano = (int) substr($data, 4, 4);
		$unixtime = mktime(0,0,0,$mes,$dia,1970);
		$dif = 1970 - $ano;
		$idade = (floor((time() - $unixtime)/31536000)) + $dif;
		return $idade;
	}
	
	//adiciona 12 horas a uma hora do tipo 07:45
	public static function plus12($hrStr) {
		$hora = explode(":", $hrStr);
		$hr = ($hora[0] >= 12) ? ($hora[0] - 12) : ($hora[0] + 12);
		$hr12 = $hr . ":" . $hora[1];
		return $hr12;
	}
			
}

?>
